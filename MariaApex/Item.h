#pragma once

#include "Hash.h"

#include <cstdint>
#include <bitset>

enum class WeaponIndex : uint32_t {
	R301 = 0,
	SENTINEL = 1,				// R301
	BOCEK = 2,					// �ڱ�
	REMPAGE_LMG = 19,			// ����
	ALTERNATOR = 75,			// ת����
	RE45,						// RE45
	CHARGE_RIFLE,				// ���ܲ�ǹ
	DEVOTION,					// רע���ǹ
	LONGBOW,					// ������ȷ��ǹ
	HAVOC,						// ���ֿ�
	EVA8_AUTO,					// Eva8
	FLATLINE,					// VK47
	G7_SCOUT,					// G7
	HEMLOK,						// ��ķ���
	KRABER,						// ���ױ���
	LSTAR,						// Lstar
	MASTIFF,					// ��Ȯ
	MOZAMBIQUE,					// Īɣ�ȿ�
	PROWLER,					// ����
	PEACEKEEPER,				// ��ƽ
	R99,						// R99
	P2020,						// P2020
	SPITFIRE,					// ������ǹ
	TRIPLE_TAKE,				// ����ʽ�ѻ�ǹ
	WINGMAN,					// ������ǹ
	VOLT,						// ����
	REPEATER,					// 30-30
	CAR,						// CAR
	NEMESIS,					// ����
	FISTS = 100,				// ȭͷ    
	THROWING_KNIFE = 139		// �ָ�׿Խ�ĵ���
};

enum class WeaponName : uint32_t {
	R301 = Utils::hash("mp_weapon_rspn101"),										// R301
	SENTINEL = Utils::hash("mp_weapon_sentinel"),									// �ڱ�
	BOCEK = Utils::hash("mp_weapon_bow"),											// ������
	MELEE_SURVIVAL = Utils::hash("mp_weapon_melee_survival"),						// ȭͷ
	REMPAGE_LMG = Utils::hash("mp_weapon_dragon_lmg"),								// ����
	ALTERNATOR = Utils::hash("mp_weapon_alternator_smg"),							// ת����
	RE45 = Utils::hash("mp_weapon_autopistol"),										// RE45
	CHARGE_RIFLE = Utils::hash("mp_weapon_defender"),								// ���ܲ�ǹ
	DEVOTION = Utils::hash("mp_weapon_esaw"),										// רע���ǹ
	LONGBOW = Utils::hash("mp_weapon_dmr"),											// ������ȷ��ǹ
	HAVOC = Utils::hash("mp_weapon_energy_ar"),										// ���ֿ�
	EVA8_AUTO = Utils::hash("mp_weapon_shotgun"),									// Eva8
	FLATLINE = Utils::hash("mp_weapon_vinson"),										// VK47
	G7_SCOUT = Utils::hash("mp_weapon_g2"),											// G7
	HEMLOK = Utils::hash("mp_weapon_hemlok"),										// ��ķ���
	KRABER = Utils::hash("mp_weapon_sniper"),										// ���ױ���
	LSTAR = Utils::hash("mp_weapon_lstar"),											// Lstar
	MASTIFF = Utils::hash("mp_weapon_mastiff"),										// ��Ȯ
	MOZAMBIQUE = Utils::hash("mp_weapon_shotgun_pistol"),							// Īɣ�ȿ�
	PROWLER = Utils::hash("mp_weapon_pdw"),											// ����
	PEACEKEEPER = Utils::hash("mp_weapon_energy_shotgun"),							// ��ƽ
	R99 = Utils::hash("mp_weapon_r97"),												// R99
	P2020 = Utils::hash("mp_weapon_semipistol"),									// P2020
	SPITFIRE = Utils::hash("mp_weapon_lmg"),										// ������ǹ
	TRIPLE_TAKE = Utils::hash("mp_weapon_doubletake"),								// ����ʽ�ѻ�ǹ
	WINGMAN = Utils::hash("mp_weapon_wingman"),										// ������ǹ
	VOLT = Utils::hash("mp_weapon_volt_smg"),										// ����
	REPEATER = Utils::hash("mp_weapon_3030"),										// 30-30
	CAR = Utils::hash("mp_weapon_car"),												// Car
	NEMESIS = Utils::hash("mp_weapon_nemesis"),										// ����Ů��
	THROWING_KNIFE = Utils::hash("mp_weapon_throwingknife"),						// �ָ�׿Խ�ĵ���
	THERMITE_GRENADE = Utils::hash("mp_weapon_thermite_grenade"),					// ���ȼ�����
	FRAG_GRENADE = Utils::hash("mp_weapon_frag_grenade"),							// ��Ƭ����
	ARC_STAR = Utils::hash("mp_weapon_grenade_emp"),								// �绡��

	EMPLACED_MINIGUN = Utils::hash("mp_weapon_mounted_turret_weapon"),
	CLUSTER_BOMB_LAUNCHER = Utils::hash("mp_weapon_cluster_bomb_launcher"),
	CONSUMABLE = Utils::hash("mp_ability_consumable")
};
