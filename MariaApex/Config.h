#pragma once
#include <string>

#include "SDK.h"

struct AimConfig
{
	ButtonCode key_code = ButtonCode::MOUSE_LEFT;	// 自瞄按键
	int smooth = 5;									// 自瞄平滑
	int bone = 2;									// 自瞄骨骼位置
	float recoil = 0.8;								// 后坐力反冲
	float active_min_distance = 50;					// 当满足这个距离时使用min_x_fov设置的fov, 换句话说当目标和本地玩家距离50米及以上时，使用较小的fov
	float active_max_distance = 6;					// 当满足这个距离时使用max_x_fov设置的fov, 换句话说当目标和本地玩家距离6米及以下时，使用较大的fov
	float min_fov = 5;
	float max_fov = 10;
};

namespace Maria
{
	class Config
	{
	public:
		bool glowing = true;						// 是否开启辉光
		bool aiming = true;							// 是否开启自瞄
		bool aim_no_recoil = true;					// 是否开启无后座
		bool observer_reminder = true;				// 是否开启观战提醒
		bool super_glide = true;					// 是否开启SuperGlide
		bool auto_reload = true;					// 是否开启AutoReload
		bool auto_firing_range = true;				// 是否开启自动检测靶场设置
		bool thirdperson = false;					// 是否开启第三人称

		bool esp = false;							// 是否开启ESP绘制总开关
		bool esp_healthbar = true;					// 是否绘制血条
		bool esp_box = true;						// 是否绘制方框
		bool esp_bone = true;						// 是否绘制骨骼
		bool esp_rank = true;						// 是否绘制段位
		bool esp_name = true;						// 是否绘制名称
		bool esp_distance = true;					// 是否绘制距离
		bool esp_level = true;						// 是否绘制等级
		bool esp_killcount = true;					// 是否绘制杀敌数

		int aim_type = 2;							// 瞄准类型，1：瞄准全部，2：瞄准可视
		float max_dist = 200.0f;					// 最大距离
		float healthbar_dist = 70.0f;				// 血条显示距离
		float glow_dist = 800.0f;					// 辉光距离

		ButtonCode superglide_key_code = ButtonCode::KEY_LSHIFT;	// SG按键

		AimConfig trackConfig;						// 自瞄配置
		AimConfig flickConfig;						// 扳机配置

		Config(std::string);
		~Config();
		void Load();
		void Save();

	private:
		std::string config_path;
	};
}
