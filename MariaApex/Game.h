#pragma once
#include "Math.h"
#include "Offsets.h"
#include "VmmProc.h"
#include "SDK.h"
#include "Item.h"
#include "Config.h"

#define NUM_ENT_ENTRIES			(1 << 16)
#define ENT_ENTRY_MASK			(NUM_ENT_ENTRIES - 1)

// 距离系数
#define UnitToMeter 0.0254f
// 显示距离转换为起源引擎世界的距离
#define EngineDist(X) X / UnitToMeter
// 起源引擎世界距离转换为现实距离
#define RealDist(X) X * UnitToMeter


typedef struct Player
{
	float dist = 0;
	int entity_team = 0;
	float boxMiddle = 0;
	float h_y = 0;
	float width = 0;
	float height = 0;
	float b_x = 0;
	float b_y = 0;
	bool knocked = false;
	bool visible = false;
	int health = 0;
	int shield = 0;
	int maxshield = 0;
	int armortype = 0;
	char name[33] = { 0 };
	int level = 0;
	int rankFlag = 0;
	int rankScore = 0;
	int killCount = 0;
} Player;

enum class AimType : uint32_t
{
	None = 0,
	Track = 1,
	Flick = 2,
};

class Entity
{
public:
	uint64_t ptr;
	uint8_t buffer[0x3FF0];
	float previousVisibleTime;
	Vector getPosition();
	bool isDummy();
	bool isPlayer();
	bool isKnocked();
	bool isAlive();
	float lastVisTime();
	uint64_t getUid();
	int getSquadId();
	int getTeamId();
	int getHealth();
	int getShield();
	int getArmorType();
	int getMaxShield();
	float getDistance(Entity& target);

	bool isGlowing();
	bool isZooming();
	bool isSameTeam(Entity& entity);

	Vector getAbsVelocity();
	QAngle getSwayAngles();
	QAngle getViewAngles();
	Vector getCamPos();
	QAngle getRecoil();
	Vector getViewAnglesV();
	float getYaw();
	std::string getSignifierName();
	float getCrosshairTargetTime();

	void enableGlow(GColor color);
	void disableGlow();
	void setViewAngles(SVector angles);
	void setViewAngles(QAngle& angles);
	Vector getBonePosition(int id);
	Vector getBonePositionByHitbox(int id);
	void getName(uint64_t index, char* name);
};

class Item
{
public:
	uint64_t ptr;
	uint8_t buffer[0x3FF0];
	Vector getPosition();
	bool isItem();
	bool isGlowing();

	void enableGlow();
	void disableGlow();
};

class WeaponXEntity
{
public:
	void update(uint64_t LocalPlayer);
	float getProjectileSpeed();
	float getProjectileGravity();
	float geZoomFov();
	int getAmmo();
	int getLatestWeapon();
	int getWeaponIndex();

private:
	float projectile_scale;
	float projectile_speed;
	float zoom_fov;
	int ammo;
	int latest_weapon;
	int weapon_index;
};


Entity GetEntity(uintptr_t ptr);
Item GetItem(uintptr_t ptr);
bool WorldToScreen(Vector from, float* m_vMatrix, int targetWidth, int targetHeight, Vector& to);
float CalculateFov(Entity& from, Entity& target);
QAngle CalculateBestBoneAim(Entity& from, uintptr_t target, AimConfig* config);
void GetClassName(uint64_t entity_ptr, char* out_str);
std::string GetLevelName();
std::string GetSignifierName(uint64_t entity_ptr);
bool IsDummy(uint64_t entity_ptr);
bool IsPlayer(uint64_t entity_ptr);
CGlobalVars GetGlobalVars();
WeaponName GetWeaponNnameByIndex(size_t index);
bool IsButtonDown(ButtonCode button_code);
std::string GetGameMode();