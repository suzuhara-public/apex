#include <thread>
#include "Main.h"
#include "Config.h"

extern Process GameProcess;
extern Maria::Config* GameConfig;

extern int g_current_fps;

void AutoReload(uint64_t pLocalEntity)
{
	int attack = Read<int>(GameProcess.hBase + OFFSET_IN_ATTACK);
	if (attack == 0)
		return;

	WeaponXEntity weapon = WeaponXEntity();
	if (pLocalEntity == 0) {
		return;
	}
	weapon.update(pLocalEntity);
	// WeaponIndex wi = static_cast<WeaponIndex>(weapon.getWeaponIndex());
	WeaponName name = GetWeaponNnameByIndex(weapon.getWeaponIndex());
	int32_t min_ammo = 0;
	switch (name)
	{
	case WeaponName::VOLT:
		min_ammo = 1;
		break;
	case WeaponName::HAVOC:
		min_ammo = 0;
		break;
	case WeaponName::LSTAR:
		return;
	case WeaponName::DEVOTION:
		min_ammo = 1;
		break;
	case WeaponName::KRABER:
		min_ammo = 0;
		break;
	case WeaponName::R99:
		min_ammo = 1;
		break;
	case WeaponName::PEACEKEEPER:
		min_ammo = 1;
		break;
	case WeaponName::SENTINEL:
		min_ammo = 0;
		break;
	case WeaponName::CHARGE_RIFLE:
		min_ammo = 0;
		break;
	case WeaponName::LONGBOW:
		min_ammo = 0;
		break;
	case WeaponName::TRIPLE_TAKE:
		min_ammo = 0;
		break;

	case WeaponName::WINGMAN:
		min_ammo = 0;
		break;
	case WeaponName::SPITFIRE:
		min_ammo = 1;
		break;
	case WeaponName::PROWLER:
		min_ammo = 0;
		break;
	case WeaponName::HEMLOK:
		min_ammo = 0;
		break;
	case WeaponName::FLATLINE:
		min_ammo = 1;
		break;

	case WeaponName::RE45:
		min_ammo = 1;
		break;
	case WeaponName::P2020:
		min_ammo = 1;
		break;
	case WeaponName::R301:
		min_ammo = 1;
		break;
	case WeaponName::CAR:
		min_ammo = 1;
		break;
	case WeaponName::ALTERNATOR:
		min_ammo = 1;
		break;
	case WeaponName::G7_SCOUT:
		min_ammo = 0;
		break;

	case WeaponName::MOZAMBIQUE:
		min_ammo = 0;
		break;
	case WeaponName::EVA8_AUTO:
		min_ammo = 1;
		break;
	case WeaponName::MASTIFF:
		min_ammo = 0;
		break;
	case WeaponName::NEMESIS:
		min_ammo = 0;
		break;
	case WeaponName::REMPAGE_LMG:
		min_ammo = 1;
		break;

	default:
		return;
	}

	int ammo = weapon.getAmmo();
	if (ammo <= min_ammo)
	{
		Write<int>(GameProcess.hBase + OFFSET_IN_ATTACK + 0x8, 4);
		Write<int>(GameProcess.hBase + OFFSET_IN_RELOAD + 0x8, 5);
		std::this_thread::sleep_for(std::chrono::milliseconds(5));
		Write<int>(GameProcess.hBase + OFFSET_IN_RELOAD + 0x8, 4);
	}
}

inline void SuperGlideJump()
{
	if (g_current_fps == 0)
		return;

	timeBeginPeriod(1);
	int sleepTime = 8;
	try
	{
		sleepTime = 1200 / g_current_fps;
		// printf("time = %d, fps = %d\n", sleepTime, g_current_fps);
	}
	catch (...) {
	}
	Write<int>(GameProcess.hBase + OFFSET_IN_JUMP + 0x8, 4);
	Write<int>(GameProcess.hBase + OFFSET_IN_JUMP + 0x8, 5);
	Sleep(sleepTime);
	Write<int>(GameProcess.hBase + OFFSET_IN_DUCK + 0x8, 5);
	Sleep(220);
	Write<int>(GameProcess.hBase + OFFSET_IN_JUMP + 0x8, 4);
	Write<int>(GameProcess.hBase + OFFSET_IN_DUCK + 0x8, 4);
	timeEndPeriod(1);
}

void SuperGlide(uint64_t pLocalEntity)
{
	static bool sg_pressed = false;
	float progress = Read<float>(pLocalEntity + OFFSET_TRAVERSAL_PROCESS);
	if (progress > 0.94) {
		if (!sg_pressed) {
			if (IsButtonDown(GameConfig->superglide_key_code))
			{
				std::thread sg(SuperGlideJump);
				sg.detach();
			}
			sg_pressed = true;
		}
	}
	else {
		sg_pressed = false;
	}
}