#pragma once

#include <Windows.h>
#include <WinUser.h>
#include <Dwmapi.h> 
#include <stdlib.h>
#include <vector>
#include <chrono>
#include <cwchar>
#include <thread>
#include <string>
#include <d3d11.h>

#include "imgui/imgui.h"
#include "imgui/imgui_impl_dx11.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/custom.h"

#include "SkCrypter.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dwmapi.lib")


#define GREEN		ImColor(0, 255, 0)				//绿色
#define RED			ImColor(255, 0, 0)				//红色
#define BLUE		ImColor(0, 0, 255)				//蓝色
#define ORANGE		ImColor(255, 165, 0)			//橙色
#define WHITE		ImColor(255, 255, 255)			//白色
#define YELLOW		ImColor(255, 255, 0)			//黄色
#define PURPLE		ImColor(255, 0, 255)			//紫色
#define CYAN		ImColor(0, 255, 255)			//青色

static int ScreenWidth = GetSystemMetrics(SM_CXSCREEN);
static int ScreenHeight = GetSystemMetrics(SM_CYSCREEN);

static bool KeyLeftClick = false;
static bool KeyIns = false;
static bool ShowMenu = false;

extern bool IsKeyDown(int vk);
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

static RECT GameRect;
static MARGINS Margin = { -1 };
static RECT rc2;
static ID3D11Device* g_pd3dDevice;
static ID3D11DeviceContext* g_pd3dDeviceContext;
static IDXGISwapChain* g_pSwapChain;
static ID3D11RenderTargetView* g_mainRenderTargetView;
static HWND GameWnd;
static HWND MyWnd;
static WNDCLASSEX wClass;
static MSG Message = { NULL };

void CleanupDeviceD3D();
bool SetupWindow();
WPARAM Draw();
HRESULT DirectXInit();
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

void Render();
void DrawMenu();
void DrawEsp();


void DrawLine(const ImVec2& x, const ImVec2 y, ImU32 color, const FLOAT width);
void DrawBox(ImColor color, float x, float y, float w, float h);
void Text(ImVec2 pos, ImColor color, const char* text_begin, const char* text_end, float wrap_width, const ImVec4* cpu_fine_clip_rect);
void DrawCircle(ImVec2 center, float radius, ImU32 col, int num_segments, float thickness);
void String(ImVec2 pos, ImColor color, const char* text);
void RectFilled(float x0, float y0, float x1, float y1, ImColor color, float rounding, int rounding_corners_flags);
void ProgressBar(float x, float y, float w, float h, int value, int v_max);
void DrawQuadFilled(ImVec2 p1, ImVec2 p2, ImVec2 p3, ImVec2 p4, ImColor color);
void DrawHexagon(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, const ImVec2& p5, const ImVec2& p6, ImU32 col, float thickness);
void DrawHexagonFilled(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, const ImVec2& p5, const ImVec2& p6, ImU32 col);
void DrawSeerLikeHealth(float x, float y, int shield, int max_shield, int armorType, int health);