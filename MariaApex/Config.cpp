#include "Config.h"
#include <fstream>
#include <iostream>
#include "Deps/json/json.hpp"

using JSON = nlohmann::json;

Maria::Config::Config(std::string path)
{
	config_path = path;
}

Maria::Config::~Config()
{
}

void Maria::Config::Load()
{
	std::ifstream file(config_path.c_str());
	JSON json = JSON::parse(file);
    aiming = json["aiming"];
    glowing = json["glowing"];
    aim_no_recoil = json["aim_no_recoil"];
    observer_reminder = json["observer_reminder"];
    super_glide = json["super_glide"];
    auto_reload = json["auto_reload"];
    auto_firing_range = json["auto_firing_range"];
    thirdperson = json["thirdperson"];
    esp = json["esp"];
    esp_healthbar = json["esp_healthbar"];
    esp_box = json["esp_box"];
    esp_bone = json["esp_bone"];
    esp_rank = json["esp_rank"];
    esp_name = json["esp_name"];
    esp_distance = json["esp_distance"];
    esp_level = json["esp_level"];
    esp_killcount = json["esp_killcount"];

    aim_type = json["aim_type"];
    max_dist = json["max_dist"];
    healthbar_dist = json["healthbar_dist"];
    glow_dist = json["glow_dist"];
    int tmp_superglide_key_code = json["superglide_key_code"];
    superglide_key_code = static_cast<ButtonCode>(tmp_superglide_key_code);

    AimConfig tmpTrackConfig;
    int tmp_key_code = json["trackConfig"]["key_code"];
    tmpTrackConfig.key_code = static_cast<ButtonCode>(tmp_key_code);
    tmpTrackConfig.smooth = json["trackConfig"]["smooth"];
    tmpTrackConfig.bone = json["trackConfig"]["bone"];
    tmpTrackConfig.recoil = json["trackConfig"]["recoil"];
    tmpTrackConfig.active_min_distance = json["trackConfig"]["active_min_distance"];
    tmpTrackConfig.active_max_distance = json["trackConfig"]["active_max_distance"];
    tmpTrackConfig.min_fov = json["trackConfig"]["min_fov"];
    tmpTrackConfig.max_fov = json["trackConfig"]["max_fov"];

    AimConfig tmpFlickConfig;
    tmp_key_code = json["flickConfig"]["key_code"];
    tmpFlickConfig.key_code = static_cast<ButtonCode>(tmp_key_code);
    tmpFlickConfig.smooth = json["flickConfig"]["smooth"];
    tmpFlickConfig.bone = json["flickConfig"]["bone"];
    tmpFlickConfig.recoil = json["flickConfig"]["recoil"];
    tmpFlickConfig.active_min_distance = json["flickConfig"]["active_min_distance"];
    tmpFlickConfig.active_max_distance = json["flickConfig"]["active_max_distance"];
    tmpFlickConfig.min_fov = json["flickConfig"]["min_fov"];
    tmpFlickConfig.max_fov = json["flickConfig"]["max_fov"];

    trackConfig = tmpTrackConfig;
    flickConfig = tmpFlickConfig;
    file.close();
}

void Maria::Config::Save()
{
    JSON json;
    json["aiming"] = aiming;
    json["glowing"] = glowing;
    json["aim_no_recoil"] = aim_no_recoil;
    json["observer_reminder"] = observer_reminder;
    json["super_glide"] = super_glide;
    json["auto_reload"] = auto_reload;
    json["auto_firing_range"] = auto_firing_range;
    json["thirdperson"] = thirdperson;
    json["esp"] = esp;
    json["esp_healthbar"] = esp_healthbar;
    json["esp_box"] = esp_box;
    json["esp_bone"] = esp_bone;
    json["esp_rank"] = esp_rank;
    json["esp_name"] = esp_name;
    json["esp_distance"] = esp_distance;
    json["esp_level"] = esp_level;
    json["esp_killcount"] = esp_killcount;

    json["aim_type"] = aim_type;
    json["max_dist"] = max_dist;
    json["healthbar_dist"] = healthbar_dist;
    json["glow_dist"] = glow_dist;
    json["superglide_key_code"] = static_cast<int>(superglide_key_code);


    json["trackConfig"]["key_code"] = static_cast<int>(trackConfig.key_code);
    json["trackConfig"]["smooth"] = trackConfig.smooth;
    json["trackConfig"]["bone"] = trackConfig.bone;
    json["trackConfig"]["recoil"] = trackConfig.recoil;
    json["trackConfig"]["active_min_distance"] = trackConfig.active_min_distance;
    json["trackConfig"]["active_max_distance"] = trackConfig.active_max_distance;
    json["trackConfig"]["min_fov"] = trackConfig.min_fov;
    json["trackConfig"]["max_fov"] = trackConfig.max_fov;

    json["flickConfig"]["key_code"] = static_cast<int>(flickConfig.key_code);
    json["flickConfig"]["smooth"] = flickConfig.smooth;
    json["flickConfig"]["bone"] = flickConfig.bone;
    json["flickConfig"]["recoil"] = flickConfig.recoil;
    json["flickConfig"]["active_min_distance"] = flickConfig.active_min_distance;
    json["flickConfig"]["active_max_distance"] = flickConfig.active_max_distance;
    json["flickConfig"]["min_fov"] = flickConfig.min_fov;
    json["flickConfig"]["max_fov"] = flickConfig.max_fov;

    std::ofstream o(config_path.c_str());
    o << std::setw(4) << json << std::endl;
    o.close();
}