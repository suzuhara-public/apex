#pragma once
#include <string>
#include <vector>
#include "pcileech/leechcore.h"
#include "pcileech/vmmdll.h"

#pragma comment(lib, "leechcore.lib")
#pragma comment(lib, "vmm.lib")  
// using namespace std;

VMM_HANDLE VMMDLL_Initialize();
void SetProcessPid(DWORD dwPid);
void SetHvmm(VMM_HANDLE hvmm);

DWORD GetProcessPid(std::string strPorName);
std::string GetPidName(DWORD dwPid);
std::string GetPidName2(DWORD dwPid);
std::vector<DWORD> GetProcessPidList();
uint64_t GetModuleFromName(std::string strName);
BOOL ReadMemory(uint64_t uBaseAddr, LPVOID lpBuffer, DWORD nSize);
BOOL ReadMemoryEx(uint64_t uBaseAddr, LPVOID lpBuffer, DWORD nSize, ULONG64 flags);
BOOL WriteMemory(uint64_t uBaseAddr, LPVOID lpBuffer, DWORD nSize);

std::vector<BYTE> ReadByte(uint64_t ptr, SIZE_T size);


template<typename T>
T Read(uint64_t ptr, bool noCache = true)
{
	T buff;
	ReadMemoryEx(ptr, &buff, sizeof(T), noCache ? 1 : 0);
	return buff;
}

template<typename T>
void ReadArray(uint64_t ptr, T out[], size_t len, bool noCache = true)
{
	ReadMemoryEx(ptr, out, sizeof(T) * len, noCache ? 1 : 0);
}

template<typename T>
T Write(uint64_t ptr, T buff)
{
	WriteMemory(ptr, &buff, sizeof(T));
	return buff;
}


bool Scatter_Read(VMMDLL_SCATTER_HANDLE HS, uint64_t addr, PVOID pBuf, DWORD size);

template<typename T>

T SRead(VMMDLL_SCATTER_HANDLE HS, uint64_t ptr)
{
	T buff;
	Scatter_Read(HS, ptr, &buff, sizeof(T));
	return buff;
}

bool SPrepare(VMMDLL_SCATTER_HANDLE HS, uint64_t va, DWORD cb);
bool SClear(VMMDLL_SCATTER_HANDLE HS, DWORD flags);
VMMDLL_SCATTER_HANDLE Scatter_Initialize(DWORD flags);
bool ExecuteRead(VMMDLL_SCATTER_HANDLE HS);



