
// [Miscellaneous]
#define OFFSET_ENTITYLIST			0x1d88fc8
#define OFFSET_LOCAL_ENT			0x0213a5c8			//.?AVC_GameMovement@@ + 0x8
#define OFFSET_NAME_LIST            0xbd28ca0
#define OFFSET_THIRDPERSON          0x01d18740 + 0x6c	//thirdperson_override + 0x6c
#define OFFSET_TIMESCALE            0x016cdc80			//host_timescale
#define OFFSET_GLOBAL_VARS          0x165c9c0			//GlobalVars
#define OFFSET_MATRIX				0x11a350			//ViewMatrix
#define OFFSET_RENDER				0x7401cd0			//ViewRender
#define OFFSET_LEVEL_NAME			0x165ce70			//LevelName
#define OFFSET_INPUT_SYSTEM			0x16d99c0			//InputSystem
#define OFFSET_VISIBLE_TIME         0x1a80				//CPlayer!lastVisibleTime
#define OFFSET_LAST_CROSSHAIR_TARGET_TIME  0x1a88				//CWeaponX!lastCrosshairTargetTime
#define OFFSET_BULLET_SPEED         0x1f50				//CWeaponX!m_flProjectileSpeed
#define OFFSET_BULLET_SCALE         0x1f58				//CWeaponX!m_flProjectileScale
#define OFFSET_STUDIOHDR            0x10f0				//CBaseAnimating!m_pStudioHdr
#define OFFSET_CAMERAPOS			0x1f58				//CPlayer!camera_origin

// [ConVars]
#define OFFSET_GAME_MODE            0x02173780			//mp_gamemode

// [NetworkedStringTables]
#define OFFSET_WEAPON_NAMES         0x074044f0			//WeaponNames


// [Buttons]
#define OFFSET_IN_ATTACK			0x074045b0			//in_attack
#define OFFSET_IN_JUMP				0x07404628			//in_jump
#define OFFSET_IN_DUCK				0x0bd296f8			//in_duck
#define OFFSET_IN_RELOAD			0x074045d0			//in_reload
#define OFFSET_IN_FORWARD			0x07404520			//in_forward

// [RecvTable.DT_Player] and [RecvTable.DT_BaseEntity]
#define OFFSET_FLAGS				0x0098				//m_fFlags
#define OFFSET_TRAVERSAL_PROCESS	0x2b5c				//m_traversalProgress
#define OFFSET_SIGNIFIER_NAME		0x0580				//m_iSignifierName
#define OFFSET_USER_ID				0x25d0				//m_platformUserId
#define OFFSET_SQUAD_ID				0x0458				//m_squadID
#define OFFSET_VIEW_MODEL           0x2d78				//m_hViewModels
#define OFFSET_TEAM					0x044c				//m_iTeamNum
#define OFFSET_HEALTH				0x043c				//m_iHealth
#define OFFSET_SHIELD				0x170				//m_shieldHealth
#define OFFSET_MAX_SHIELD           0x0174				//m_shieldHealthMax
#define OFFSET_NAME					0x589				//m_iName
#define OFFSET_SIGN_NAME			0x580				//m_iSignifierName
#define OFFSET_ABS_VELOCITY         0x140				//m_vecAbsVelocity
#define OFFSET_ZOOMING      		0x1c61				//m_bZooming
#define OFFSET_THIRDPERSON_SV       0x36e0				//m_thirdPersonShoulderView
#define OFFSET_YAW                  0x22c4 - 0x8		//m_currentFramePlayer.m_ammoPoolCount - 0x8
#define OFFSET_SKYDIVE_STATE		0x46a4				//m_skydiveState
#define OFFSET_LIFE_STATE			0x798				//m_lifeState, >0 = dead
#define OFFSET_BLEED_OUT_STATE		0x2750				//m_bleedoutState, >0 = knocked
#define OFFSET_ORIGIN				0x014c				//m_vecAbsOrigin
#define OFFSET_AIMPUNCH				0x24c0				//m_currentFrameLocalPlayer.m_vecPunchWeapon_Angle
#define OFFSET_VIEWANGLES			0x25bc - 0x14		//m_ammoPoolCapacity - 0x14
#define OFFSET_BREATH_ANGLES		OFFSET_VIEWANGLES - 0x10
#define OFFSET_OBSERVER_MODE		0x34ec				//m_iObserverMode
#define OFFSET_OBSERVING_TARGET		0x34f8				//m_hObserverTarget
#define OFFSET_ARMOR_TYPE           0x464c				//armortype

// [RecvTable.DT_BaseAnimating]
#define OFFSET_BONES				0x0e98 + 0x48		//m_nForceBone + 0x48

// [DataMap.C_BaseCombatCharacter]
#define OFFSET_PRIMARY_WEAPON		0x1a24				//DataMap.C_BaseCombatCharacter.m_latestPrimaryWeapons
#define OFFSET_CURRENT_WEAPON		OFFSET_PRIMARY_WEAPON - 0x14

// [RecvTable.DT_WeaponX]
#define OFFSET_ZOOM_FOV             0x16c0 + 0xb8		//m_playerData + m_curZoomFOV
#define OFFSET_AMMO                 0x1670				//m_ammoInClip
#define OFFSET_WEAPON_NAME_INDEX			0x1870				//m_weaponNameIndex


// [FIXED]
#define OFFSET_INPUT_BUTTON_STATE	0xb0				//InputSystemState

#define OFFSET_ITEM_GLOW            0x2c0				//m_highlightFunctionBits
#define OFFSET_GLOW_T1              0x262				//16256 = enabled, 0 = disabled 
#define OFFSET_GLOW_T2              0x2dc				//1193322764 = enabled, 0 = disabled 
#define OFFSET_GLOW_ENABLE          0x3c8				//7 = enabled, 2 = disabled
#define OFFSET_GLOW_THROUGH_WALLS   0x3d0				//2 = enabled, 5 = disabled

#define OFFSET_GLOW_ENABLE_GLOW_CONTEXT                 OFFSET_GLOW_ENABLE			// Script_Highlight_SetCurrentContext
#define OFFSET_GLOW_THROUGH_WALLS_GLOW_VISIBLE_TYPE     OFFSET_GLOW_THROUGH_WALLS	// Script_Highlight_SetVisibilityType 5th mov
#define GLOW_LIFE_TIME              0x3A4 // Script_Highlight_SetLifeTime + 4
#define GLOW_DISTANCE               0x3B4 // Script_Highlight_SetFarFadeDist
#define GLOW_TYPE                   0x2C4 // Script_Highlight_GetState + 4
#define GLOW_COLOR                  0x1D0 // Script_CopyHighlightState 15th mov
#define GLOW_FADE                   0x388 // Script_Highlight_GetCurrentInsideOpacity 3rd result of 3 offsets consecutive or first + 8
