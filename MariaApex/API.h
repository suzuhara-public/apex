#pragma once

#include <iostream>
#include "Deps/json/json.hpp"
#include "Deps/curl/curl.h"

#pragma comment ( lib, "libssl.lib" )
#pragma comment ( lib, "libcurl.lib" )


typedef struct PlayerDetail
{
	int level = 0;
	int rankFlag;
	int rankScore = 0;
	int rankPos = 0;
} PlayerDetail;


static inline size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp) {
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

inline std::string GetApiAuthKey()
{
	return "e3e1267ce7e4540b47e2c0d24052f619";
}

/*
 * 通过api查询获取玩家详细数据
 */
std::string GetPlayerDetailFromApi(uint64_t uid) {
	CURL* curl;
	CURLcode res;
	std::string readBuffer;

	auto key = GetApiAuthKey();
	std::string url = "https://api.mozambiquehe.re/bridge?auth=" + key + "&uid=" + std::to_string(uid) + "&platform=PC";

	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
	}
	return readBuffer;
}

inline int GetRankFlag(std::string rankName)
{
	int rank = 0;

	if (rankName == "Rookie") {
		rank = 1;
	}
	else if (rankName == "Bronze") {
		rank = 2;
	}
	else if (rankName == "Silver") {
		rank = 3;
	}
	else if (rankName == "Gold") {
		rank = 4;
	}
	else if (rankName == "Platinum") {
		rank = 5;
	}
	else if (rankName == "Diamond") {
		rank = 6;
	}
	else if (rankName == "Master") {
		rank = 7;
	}
	else if (rankName == "Apex Predator") {
		rank = 8;
	}
	return rank;
}

PlayerDetail* GetPlayerDetail(uint64_t uid)
{
	std::string res = GetPlayerDetailFromApi(uid);
	nlohmann::json result = nlohmann::json::parse(res);
	std::string rankName = result["global"]["rank"]["rankName"];
	int level = result["global"]["level"];
	int prestige = result["global"]["levelPrestige"];
	int rankScore = result["global"]["rank"]["rankScore"];
	int rankPos = result["global"]["rank"]["ladderPosPlatform"];

	int rankFlag = GetRankFlag(rankName);
	PlayerDetail* detail = new PlayerDetail();
	detail->level = level;
	detail->rankFlag = rankFlag;
	detail->rankScore = rankScore;
	detail->rankPos = rankPos;
	return detail;
}