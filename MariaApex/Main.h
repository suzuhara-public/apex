#pragma once

#include <stdio.h>
#include <thread>
#include <iostream>
#include <sys/types.h>
#include <string.h>
#include <chrono>
#include <map>

#include "VmmProc.h"
#include "Offsets.h"
#include "Game.h"
#include "Item.h"
#include "SkCrypter.h"
#include "Overlay.h"
#include "Scripts.h"
#include "Config.h"


#pragma comment(lib, "winmm.lib")

typedef struct Process
{
	VMM_HANDLE hVmm;
	DWORD hProcess;
	uint64_t hBase;
}Process;


// #############常量###############
const int ReadEntitySize = 64;					// 非靶场循环实体数量
const int FiringRangeReadEntitySize = 10000;	// 靶场循环实体数量
const double ObserverRefreshDurationTime = 3000;// 观战人数刷新时间
