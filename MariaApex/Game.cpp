#include <limits>
#include "Prediction.h"
#include "Main.h"
#include "Game.h"

extern Process GameProcess;
extern Maria::Config* GameConfig;

extern bool firing_range;
extern std::vector<std::string> g_weapon_names;
extern uint32_t button_state[4];


std::string GetLevelName()
{
	char levelName[32];
	ReadArray<char>(GameProcess.hBase + OFFSET_LEVEL_NAME, levelName, 32, false);
	return std::string(levelName);
}

uint64_t Entity::getUid()
{
	return *(uint64_t*)(buffer + OFFSET_USER_ID);
}

int Entity::getSquadId()
{
	return *(int*)(buffer + OFFSET_SQUAD_ID);
}

int Entity::getTeamId()
{
	return *(int*)(buffer + OFFSET_TEAM);
}

int Entity::getHealth()
{
	return *(int*)(buffer + OFFSET_HEALTH);
}

int Entity::getShield()
{
	return *(int*)(buffer + OFFSET_SHIELD);
}

int Entity::getMaxShield()
{
	return *(int*)(buffer + OFFSET_MAX_SHIELD);
}

float Entity::getDistance(Entity& target) {
	Vector localPos = getPosition();
	Vector targetPosition = target.getPosition();
	return RealDist(localPos.DistTo(targetPosition));
}

int Entity::getArmorType()
{
	return Read<int>(ptr + OFFSET_ARMOR_TYPE);
}

Vector Entity::getAbsVelocity()
{
	return *(Vector*)(buffer + OFFSET_ABS_VELOCITY);
}

Vector Entity::getPosition()
{
	return *(Vector*)(buffer + OFFSET_ORIGIN);
}

bool Entity::isPlayer()
{
	return *(uint64_t*)(buffer + OFFSET_NAME) == 125780153691248;
}

bool Entity::isDummy()
{
	std::string name = getSignifierName();
	return  name == "npc_dummie";
	/*char class_name[33] = {};
	GetClassName(ptr, class_name);
	return strncmp(class_name, "CAI_BaseNPC", 11) == 0;*/
}

bool Entity::isKnocked()
{
	return *(int*)(buffer + OFFSET_BLEED_OUT_STATE) > 0;
}

bool Entity::isAlive()
{
	return *(int*)(buffer + OFFSET_LIFE_STATE) == 0;
}

float Entity::lastVisTime()
{
	// return Read<float>(ptr + OFFSET_VISIBLE_TIME);
	return *(float*)(buffer + OFFSET_VISIBLE_TIME);
}

std::string Entity::getSignifierName()
{
	uint64_t signifierName = *(uint64_t*)(buffer + OFFSET_SIGNIFIER_NAME);
	char name[32];
	ReadArray<char>(signifierName, name, 32, false);
	return std::string(name);
}

float Entity::getCrosshairTargetTime()
{
	return *(float*)(buffer + OFFSET_LAST_CROSSHAIR_TARGET_TIME);
}


Vector Entity::getBonePosition(int id)
{
	Vector position = getPosition();
	uintptr_t boneArray = *(uintptr_t*)(buffer + OFFSET_BONES);
	Vector bone = Vector();
	uint32_t boneloc = (id * 0x30);
	Bone bo = Read<Bone>(boneArray + boneloc);
	bone.x = bo.x + position.x;
	bone.y = bo.y + position.y;
	bone.z = bo.z + position.z;
	return bone;
}

//https://www.unknowncheats.me/forum/apex-legends/496984-getting-hitbox-positions-cstudiohdr-externally.html
//https://www.unknowncheats.me/forum/3499185-post1334.html
//https://www.unknowncheats.me/forum/3562047-post11000.html
Vector Entity::getBonePositionByHitbox(int id)
{
	Vector origin = getPosition();

	//BoneByHitBox
	uint64_t Model = *(uint64_t*)(buffer + OFFSET_STUDIOHDR);

	//get studio hdr
	uint64_t StudioHdr = Read<uint64_t>(Model + 0x8);

	//get hitbox array
	uint16_t HitboxCache = Read<uint16_t>(StudioHdr + 0x34);
	uint64_t HitBoxsArray = StudioHdr + ((uint16_t)(HitboxCache & 0xFFFE) << (4 * (HitboxCache & 1)));

	uint16_t IndexCache = Read<uint16_t>(HitBoxsArray + 0x4);
	int HitboxIndex = ((uint16_t)(IndexCache & 0xFFFE) << (4 * (IndexCache & 1)));

	uint16_t Bone = Read<uint16_t>(HitBoxsArray + HitboxIndex + (id * 0x20));

	if (Bone < 0 || Bone > 255)
		return Vector();

	//hitpos
	uint64_t BoneArray = *(uint64_t*)(buffer + OFFSET_BONES);

	matrix3x4_t Matrix = Read<matrix3x4_t>(BoneArray + Bone * sizeof(matrix3x4_t));

	return Vector(Matrix.m_flMatVal[0][3] + origin.x, Matrix.m_flMatVal[1][3] + origin.y, Matrix.m_flMatVal[2][3] + origin.z);
}

QAngle Entity::getSwayAngles()
{
	return *(QAngle*)(buffer + OFFSET_BREATH_ANGLES);
}

QAngle Entity::getViewAngles()
{
	return *(QAngle*)(buffer + OFFSET_VIEWANGLES);
}

Vector Entity::getViewAnglesV()
{
	return *(Vector*)(buffer + OFFSET_VIEWANGLES);
}

float Entity::getYaw()
{
	float yaw = Read<float>(ptr + OFFSET_YAW);

	if (yaw < 0)
		yaw += 360;
	yaw += 90;
	if (yaw > 360)
		yaw -= 360;

	return yaw;
}

bool Entity::isGlowing()
{
	return *(int*)(buffer + OFFSET_GLOW_ENABLE) == 1;
}

bool Entity::isZooming()
{
	return *(int*)(buffer + OFFSET_ZOOMING) == 1;
}


bool Entity::isSameTeam(Entity& entity)
{
	std::string mode = GetGameMode();
	int id = getSquadId();
	int localTeam = getTeamId();
	int entityTeam = entity.getTeamId();
	if (mode == "freedm")									// 团队/子弹时间等
	{
		if (id != -1)
		{
			return localTeam == entityTeam;
		}
		else
		{
			return (localTeam & 1) == (entityTeam & 1);
		}
	}
	else if (mode == "control")								// 控制模式
	{
		return (localTeam & 1) == (entityTeam & 1);
	} 

	return localTeam == entityTeam;							// 大逃杀
}

void Entity::enableGlow(GColor color)
{
	// Write<int>(ptr + OFFSET_GLOW_T1, 16256);
	// Write<int>(ptr + OFFSET_GLOW_T2, 1193322764);
	Write<GlowMode>(ptr + GLOW_TYPE, { 101,101,70,83 });
	Write<float>(ptr + GLOW_DISTANCE, EngineDist(GameConfig->glow_dist));
	Write<GColor>(ptr + GLOW_COLOR, color);
	Write<int>(ptr + OFFSET_GLOW_ENABLE, 1);
	Write<int>(ptr + OFFSET_GLOW_THROUGH_WALLS, 2);
}

void Entity::disableGlow()
{
	Write<int>(ptr + OFFSET_GLOW_T1, 0);
	Write<int>(ptr + OFFSET_GLOW_T2, 0);
	Write<int>(ptr + OFFSET_GLOW_ENABLE, 2);
	Write<int>(ptr + OFFSET_GLOW_THROUGH_WALLS, 5);
}

void Entity::setViewAngles(SVector angles)
{
	Write<SVector>(ptr + OFFSET_VIEWANGLES, angles);
}

void Entity::setViewAngles(QAngle& angles)
{
	setViewAngles(SVector(angles));
}

Vector Entity::getCamPos()
{
	return *(Vector*)(buffer + OFFSET_CAMERAPOS);
}

QAngle Entity::getRecoil()
{
	return *(QAngle*)(buffer + OFFSET_AIMPUNCH);
}

void Entity::getName(uint64_t index, char* name)
{
	index *= 0x10;
	uint64_t name_ptr = Read<uint64_t>(GameProcess.hBase + OFFSET_NAME_LIST + index);
	ReadArray<char>(name_ptr, name, 32);
}


bool Item::isItem()
{
	char class_name[33] = {};
	GetClassName(ptr, class_name);

	return strncmp(class_name, "CPropSurvival", 13) == 0;
}

bool Item::isGlowing()
{
	return *(int*)(buffer + OFFSET_ITEM_GLOW) == 1363184265;
}

void Item::enableGlow()
{
	Write<int>(ptr + OFFSET_ITEM_GLOW, 1363184265);
}

void Item::disableGlow()
{
	Write<int>(ptr + OFFSET_ITEM_GLOW, 1411417991);
}

Vector Item::getPosition()
{
	return *(Vector*)(buffer + OFFSET_ORIGIN);
}

Item GetItem(uintptr_t ptr)
{
	Item entity = Item();
	entity.ptr = ptr;
	ReadArray<uint8_t>(ptr, entity.buffer, sizeof(entity.buffer));
	return entity;
}

void WeaponXEntity::update(uint64_t LocalPlayer)
{
	uint64_t g_Base = GameProcess.hBase;
	uint64_t entitylist = g_Base + OFFSET_ENTITYLIST;
	uint64_t wephandle = Read<uint64_t>(LocalPlayer + OFFSET_PRIMARY_WEAPON);

	wephandle &= 0xffff;

	uint64_t wep_entity = Read<uint64_t>(entitylist + (wephandle << 5));

	projectile_speed = Read<float>(wep_entity + OFFSET_BULLET_SPEED);
	projectile_scale = Read<float>(wep_entity + OFFSET_BULLET_SCALE);
	zoom_fov = Read<float>(wep_entity + OFFSET_ZOOM_FOV);
	ammo = Read<int>(wep_entity + OFFSET_AMMO);

	weapon_index = Read<int>(wep_entity + OFFSET_WEAPON_NAME_INDEX);
	latest_weapon = Read<int>(LocalPlayer + OFFSET_CURRENT_WEAPON);
}

float WeaponXEntity::getProjectileSpeed()
{
	return projectile_speed;
}

float WeaponXEntity::getProjectileGravity()
{
	return 750.0f * projectile_scale;
}

float WeaponXEntity::geZoomFov()
{
	return zoom_fov;
}

int WeaponXEntity::getAmmo()
{
	return ammo;
}

int WeaponXEntity::getLatestWeapon()
{
	return latest_weapon;
}

int WeaponXEntity::getWeaponIndex()
{
	return weapon_index;
}

Entity GetEntity(uintptr_t ptr)
{
	Entity entity = Entity();
	entity.ptr = ptr;
	ReadArray<uint8_t>(ptr, entity.buffer, sizeof(entity.buffer));
	return entity;
}

//https://github.com/CasualX/apexbot/blob/master/src/state.cpp#L104
void GetClassName(uint64_t entity_ptr, char* out_str)
{
	uint64_t client_networkable_vtable = Read<uint64_t>(entity_ptr + 8 * 3, false);
	uint64_t get_client_class = Read<uint64_t>(client_networkable_vtable + 8 * 3, false);
	uint32_t disp = Read<uint32_t>(get_client_class + 3, false);

	const uint64_t client_class_ptr = get_client_class + disp + 7;

	ClientClass client_class = Read<ClientClass>(client_class_ptr, false);

	ReadArray<char>(client_class.pNetworkName, out_str, 32, false);
}

bool WorldToScreen(Vector from, float* m_vMatrix, int targetWidth, int targetHeight, Vector& to)
{
	float w = m_vMatrix[12] * from.x + m_vMatrix[13] * from.y + m_vMatrix[14] * from.z + m_vMatrix[15];

	if (w < 0.01f) return false;

	to.x = m_vMatrix[0] * from.x + m_vMatrix[1] * from.y + m_vMatrix[2] * from.z + m_vMatrix[3];
	to.y = m_vMatrix[4] * from.x + m_vMatrix[5] * from.y + m_vMatrix[6] * from.z + m_vMatrix[7];

	float invw = 1.0f / w;
	to.x *= invw;
	to.y *= invw;

	float x = targetWidth / 2;
	float y = targetHeight / 2;

	x += 0.5 * to.x * targetWidth + 0.5;
	y -= 0.5 * to.y * targetHeight + 0.5;

	to.x = x;
	to.y = y;
	to.z = 0;
	return true;
}

float CalculateFov(Entity& from, Entity& target)
{
	QAngle ViewAngles = from.getViewAngles();
	Vector LocalCamera = from.getCamPos();
	Vector EntityPosition = target.getPosition();
	QAngle Angle = Math::CalcAngle(LocalCamera, EntityPosition);
	return Math::GetFov(ViewAngles, Angle);
}
QAngle CalculateBestBoneAim(Entity& from, uintptr_t t, AimConfig* config)
{
	int max_fov = config->max_fov;
	int bone = config->bone;
	int smooth = config->smooth;
	float recoil = config->recoil;

	Entity target = GetEntity(t);
	if (firing_range)
	{
		if (!target.isAlive())
		{
			return QAngle(0, 0, 0);
		}
	}
	else
	{
		if (!target.isAlive() || target.isKnocked())
		{
			return QAngle(0, 0, 0);
		}
	}

	Vector LocalCamera = from.getCamPos();
	Vector TargetBonePosition = target.getBonePositionByHitbox(bone);
	QAngle CalculatedAngles = QAngle(0, 0, 0);

	WeaponXEntity curweap = WeaponXEntity();
	curweap.update(from.ptr);
	float BulletSpeed = curweap.getProjectileSpeed();
	float BulletGrav = curweap.getProjectileGravity();
	float zoom_fov = curweap.geZoomFov();

	if (zoom_fov != 0.0f && zoom_fov != 1.0f)
	{
		max_fov *= zoom_fov / 90.0f;
	}

	//more accurate prediction
	if (BulletSpeed > 1.f)
	{
		PredictCtx Ctx;
		Ctx.StartPos = LocalCamera;
		Ctx.TargetPos = TargetBonePosition;
		Ctx.BulletSpeed = BulletSpeed - (BulletSpeed * 0.08);
		Ctx.BulletGravity = BulletGrav + (BulletGrav * 0.05);
		Ctx.TargetVel = target.getAbsVelocity();

		if (BulletPredict(Ctx))
			CalculatedAngles = QAngle{ Ctx.AimAngles.x, Ctx.AimAngles.y, 0.f };
	}

	if (CalculatedAngles == QAngle(0, 0, 0))
		CalculatedAngles = Math::CalcAngle(LocalCamera, TargetBonePosition);

	QAngle ViewAngles = from.getViewAngles();
	QAngle SwayAngles = from.getSwayAngles();
	//remove sway and recoil
	CalculatedAngles -= (SwayAngles - ViewAngles) * recoil;
	Math::NormalizeAngles(CalculatedAngles);
	QAngle Delta = CalculatedAngles - ViewAngles;
	double fov = Math::GetFov(SwayAngles, CalculatedAngles);
	if (fov > max_fov)
	{
		return QAngle(0, 0, 0);
	}

	Math::NormalizeAngles(Delta);
	QAngle SmoothedAngles = ViewAngles + Delta / smooth;
	return SmoothedAngles;
}

std::string GetSignifierName(uint64_t entity_ptr)
{
	uint64_t signifierName = Read<uint64_t>(entity_ptr + OFFSET_SIGNIFIER_NAME, false);
	char name[32];
	ReadArray<char>(signifierName, name, 32, false);
	return std::string(name);
}

bool IsDummy(uint64_t entity_ptr)
{
	return GetSignifierName(entity_ptr) == "npc_dummie";
}

bool IsPlayer(uint64_t entity_ptr)
{
	return GetSignifierName(entity_ptr) == "player";
}

CGlobalVars GetGlobalVars() 
{
	return Read<CGlobalVars>(GameProcess.hBase + OFFSET_GLOBAL_VARS);
}

/*
 * 通过索引获取武器名称
 */
WeaponName GetWeaponNnameByIndex(size_t index)
{
	// size_t index = static_cast<uint32_t>(weapon_index);
	return static_cast<WeaponName>(index < g_weapon_names.size() ? Utils::hash(g_weapon_names[index].c_str()) : 0);
}

// 判断按键是否按下
bool IsButtonDown(ButtonCode button_code) {
	return (button_state[static_cast<uint32_t>(button_code) >> 5] & (1 << (static_cast<uint32_t>(button_code) & 0x1f))) != 0;
}

std::string GetGameMode()
{
	uint64_t gameModePtr = Read<uint64_t>(GameProcess.hBase + OFFSET_GAME_MODE, false);
	char name[32];
	ReadArray<char>(gameModePtr, name, 32, false);
	return std::string(name);
}