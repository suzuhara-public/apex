#include <fstream>

#include "Overlay.h"
#include "Main.h"
#include "Config.h"

extern Process GameProcess;
extern Maria::Config* GameConfig;

extern bool firing_range;
extern Player g_players[ReadEntitySize];

extern int track_key;
extern int flick_key;
extern std::map<int, ButtonCode> g_aim_key_mapping;

#pragma region Draw

void DrawLine(const ImVec2& x, const ImVec2 y, ImU32 color, const FLOAT width)
{
    ImGui::GetBackgroundDrawList()->AddLine(x, y, color, width);
}

void DrawBox(ImColor color, float x, float y, float w, float h)
{
    DrawLine(ImVec2(x, y), ImVec2(x + w, y), color, 1.0f);
    DrawLine(ImVec2(x, y), ImVec2(x, y + h), color, 1.0f);
    DrawLine(ImVec2(x + w, y), ImVec2(x + w, y + h), color, 1.0f);
    DrawLine(ImVec2(x, y + h), ImVec2(x + w, y + h), color, 1.0f);
}

void Text(ImVec2 pos, ImColor color, const char* text_begin, const char* text_end, float wrap_width, const ImVec4* cpu_fine_clip_rect)
{
    ImGui::GetBackgroundDrawList()->AddText(ImGui::GetFont(), ImGui::GetFontSize(), pos, color, text_begin, text_end, wrap_width, cpu_fine_clip_rect);
}
void DrawCircle(ImVec2 center, float radius, ImU32 col, int num_segments, float thickness)
{
    ImGui::GetForegroundDrawList()->AddCircle(center, radius, col, num_segments, thickness);
}
void String(ImVec2 pos, ImColor color, const char* text)
{
    Text(pos, color, text, text + strlen(text), 200, 0);
}

void RectFilled(float x0, float y0, float x1, float y1, ImColor color, float rounding, int rounding_corners_flags)
{
    ImGui::GetBackgroundDrawList()->AddRectFilled(ImVec2(x0, y0), ImVec2(x1, y1), color, rounding, rounding_corners_flags);
}

void ProgressBar(float x, float y, float w, float h, int value, int v_max)
{
    ImColor barColor = ImColor(
        min(510 * (v_max - value) / 100, 255),
        min(510 * value / 100, 255),
        25,
        255
    );

    RectFilled(x, y, x + w, y + ((h / float(v_max)) * (float)value), barColor, 0.0f, 0);
}


//Seer Hp and Shield bars (never re fixed the armor type so its set to max shield)

void DrawQuadFilled(ImVec2 p1, ImVec2 p2, ImVec2 p3, ImVec2 p4, ImColor color) {
    ImGui::GetBackgroundDrawList()->AddQuadFilled(p1, p2, p3, p4, color);
}
void DrawHexagon(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, const ImVec2& p5, const ImVec2& p6, ImU32 col, float thickness)
{
    ImGui::GetBackgroundDrawList()->AddHexagon(p1, p2, p3, p4, p5, p6, col, thickness);
}
void DrawHexagonFilled(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, const ImVec2& p5, const ImVec2& p6, ImU32 col)
{
    ImGui::GetBackgroundDrawList()->AddHexagonFilled(p1, p2, p3, p4, p5, p6, col);
}

void DrawSeerLikeHealth(float x, float y, int shield, int max_shield, int armorType, int health) {

    int bg_offset = 3;
    int bar_width = 78;
    // 4steps...2*3=6
    // 38*4=152 152+6 = 158
    // 5steps...2*4=8
    // 30*5=150 150+8 = 158
    float max_health = 100.0f;
    float shield_step = 25.0f;

    int shield_25 = 14;
    int steps = 5;


    ImVec2 bg1(x - bar_width / 2 - bg_offset, y);
    ImVec2 bg2(bg1.x - 10, bg1.y - 16);
    ImVec2 bg3(bg2.x + 5, bg2.y - 7);
    ImVec2 bg4(bg3.x + bar_width + bg_offset, bg3.y);
    ImVec2 bg5(bg4.x + 11, bg4.y + 18);
    ImVec2 bg6(x + bar_width / 2 + bg_offset, y);
    //DrawHexagonFilled(bg1, bg2, bg3, bg4, bg5, bg6, ImColor(0, 0, 0, 120));


    ImVec2 h1(bg1.x + 3, bg1.y - 4);
    ImVec2 h2(h1.x - 5, h1.y - 8);
    ImVec2 h3(h2.x + (float)health / max_health * bar_width, h2.y);
    ImVec2 h4(h1.x + (float)health / max_health * bar_width, h1.y);
    ImVec2 h3m(h2.x + bar_width, h2.y);
    ImVec2 h4m(h1.x + bar_width, h1.y);
    DrawQuadFilled(h1, h2, h3m, h4m, ImColor(10, 10, 30, 60));
    DrawQuadFilled(h1, h2, h3, h4, WHITE);


    ImColor shieldCracked(0, 0, 0);
    ImColor shieldCrackedDark(0, 0, 0);

    ImColor shieldCol;
    ImColor shieldColDark; //not used, but the real seer q has shadow inside
    if (max_shield == 50) { //white
        shieldCol = ImColor(247, 247, 247);
        shieldColDark = ImColor(164, 164, 164);
    }
    else if (max_shield == 75) { //blue
        shieldCol = ImColor(39, 178, 255);
        shieldColDark = ImColor(27, 120, 210);
    }

    else if (max_shield == 100) { //purple
        shieldCol = ImColor(206, 59, 255);
        shieldColDark = ImColor(136, 36, 220);
    }
    else if (max_shield == 100) { //gold
        shieldCol = ImColor(255, 255, 79);
        shieldColDark = ImColor(218, 175, 49);
    }

    else if (max_shield == 125) { //red
        shieldCol = ImColor(219, 2, 2);
        shieldColDark = ImColor(219, 2, 2);
    }
    else {
        shieldCol = ImColor(247, 247, 247);
        shieldColDark = ImColor(164, 164, 164);
    }
    int shield_tmp = shield;
    int shield1 = 0;
    int shield2 = 0;
    int shield3 = 0;
    int shield4 = 0;
    int shield5 = 0;
    if (shield_tmp > 25) {
        shield1 = 25;
        shield_tmp -= 25;
        if (shield_tmp > 25) {
            shield2 = 25;
            shield_tmp -= 25;
            if (shield_tmp > 25) {
                shield3 = 25;
                shield_tmp -= 25;
                if (shield_tmp > 25) {
                    shield4 = 25;
                    shield_tmp -= 25;
                    shield5 = shield_tmp;
                }
                else {
                    shield4 = shield_tmp;
                }
            }
            else {
                shield3 = shield_tmp;
            }
        }
        else {
            shield2 = shield_tmp;
        }
    }
    else {
        shield1 = shield_tmp;
    }
    ImVec2 s1(h2.x - 1, h2.y - 2);
    ImVec2 s2(s1.x - 3, s1.y - 5);
    ImVec2 s3(s2.x + shield1 / shield_step * shield_25, s2.y);
    ImVec2 s4(s1.x + shield1 / shield_step * shield_25, s1.y);
    ImVec2 s3m(s2.x + shield_25, s2.y);
    ImVec2 s4m(s1.x + shield_25, s1.y);

    ImVec2 ss1(s4m.x + 2, s1.y);
    ImVec2 ss2(s3m.x + 2, s2.y);
    ImVec2 ss3(ss2.x + shield2 / shield_step * shield_25, s2.y);
    ImVec2 ss4(ss1.x + shield2 / shield_step * shield_25, s1.y);
    ImVec2 ss3m(ss2.x + shield_25, s2.y);
    ImVec2 ss4m(ss1.x + shield_25, s1.y);

    ImVec2 sss1(ss4m.x + 2, s1.y);
    ImVec2 sss2(ss3m.x + 2, s2.y);
    ImVec2 sss3(sss2.x + shield3 / shield_step * shield_25, s2.y);
    ImVec2 sss4(sss1.x + shield3 / shield_step * shield_25, s1.y);
    ImVec2 sss3m(sss2.x + shield_25, s2.y);
    ImVec2 sss4m(sss1.x + shield_25, s1.y);

    ImVec2 ssss1(sss4m.x + 2, s1.y);
    ImVec2 ssss2(sss3m.x + 2, s2.y);
    ImVec2 ssss3(ssss2.x + shield4 / shield_step * shield_25, s2.y);
    ImVec2 ssss4(ssss1.x + shield4 / shield_step * shield_25, s1.y);
    ImVec2 ssss3m(ssss2.x + shield_25, s2.y);
    ImVec2 ssss4m(ssss1.x + shield_25, s1.y);

    ImVec2 sssss1(ssss4m.x + 2, s1.y);
    ImVec2 sssss2(ssss3m.x + 2, s2.y);
    ImVec2 sssss3(sssss2.x + shield5 / shield_step * shield_25, s2.y);
    ImVec2 sssss4(sssss1.x + shield5 / shield_step * shield_25, s1.y);
    ImVec2 sssss3m(sssss2.x + shield_25, s2.y);
    ImVec2 sssss4m(sssss1.x + shield_25, s1.y);
    if (max_shield == 50) {
        if (shield <= 25) {
            if (shield < 25) {
                DrawQuadFilled(s1, s2, s3m, s4m, shieldCracked);
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(s1, s2, s3, s4, shieldCol);

        }
        else if (shield <= 50) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            if (shield != 50) {
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
        }
    }
    else if (max_shield == 75) {
        if (shield <= 25) {
            if (shield < 25) {
                DrawQuadFilled(s1, s2, s3m, s4m, shieldCracked);
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(s1, s2, s3, s4, shieldCol);

        }
        else if (shield <= 50) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            if (shield < 50) {
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
        }
        else if (shield <= 75) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
            if (shield < 75) {
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(sss1, sss2, sss3, sss4, shieldCol);
        }
    }
    else if (max_shield == 100) {
        if (shield <= 25) {
            if (shield < 25) {
                DrawQuadFilled(s1, s2, s3m, s4m, shieldCracked);
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(s1, s2, s3, s4, shieldCol);

        }
        else if (shield <= 50) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            if (shield < 50) {
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
        }
        else if (shield <= 75) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
            if (shield < 75) {
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(sss1, sss2, sss3, sss4, shieldCol);
        }
        else if (shield <= 100) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
            DrawQuadFilled(sss1, sss2, sss3, sss4, shieldCol);
            if (shield < 100) {
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(ssss1, ssss2, ssss3, ssss4, shieldCol);
        }
    }
    else if (max_shield == 125) {
        if (shield <= 25) {
            if (shield < 25) {
                DrawQuadFilled(s1, s2, s3m, s4m, shieldCracked);
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
                DrawQuadFilled(sssss1, sssss2, sssss3m, sssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(s1, s2, s3, s4, shieldCol);

        }
        else if (shield <= 50) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            if (shield < 50) {
                DrawQuadFilled(ss1, ss2, ss3m, ss4m, shieldCracked);
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
                DrawQuadFilled(sssss1, sssss2, sssss3m, sssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
        }
        else if (shield <= 75) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
            if (shield < 75) {
                DrawQuadFilled(sss1, sss2, sss3m, sss4m, shieldCracked);
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
                DrawQuadFilled(sssss1, sssss2, sssss3m, sssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(sss1, sss2, sss3, sss4, shieldCol);
        }
        else if (shield <= 100) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
            DrawQuadFilled(sss1, sss2, sss3, sss4, shieldCol);
            if (shield < 100) {
                DrawQuadFilled(ssss1, ssss2, ssss3m, ssss4m, shieldCracked);
                DrawQuadFilled(sssss1, sssss2, sssss3m, sssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(ssss1, ssss2, ssss3, ssss4, shieldCol);
        }
        else if (shield <= 125) {
            DrawQuadFilled(s1, s2, s3, s4, shieldCol);
            DrawQuadFilled(ss1, ss2, ss3, ss4, shieldCol);
            DrawQuadFilled(sss1, sss2, sss3, sss4, shieldCol);
            DrawQuadFilled(ssss1, ssss2, ssss3, ssss4, shieldCol);
            if (shield < 125) {
                DrawQuadFilled(sssss1, sssss2, sssss3m, sssss4m, shieldCracked);
            }
            if (shield != 0)
                DrawQuadFilled(sssss1, sssss2, sssss3, sssss4, shieldCol);
        }
    }
}

#pragma endregion

#pragma region CreateDevice

void CreateRenderTarget()
{
    ID3D11Texture2D* pBackBuffer;
    g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
    g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_mainRenderTargetView);
    pBackBuffer->Release();
}

void CleanupRenderTarget()
{
    if (g_mainRenderTargetView) { g_mainRenderTargetView->Release(); g_mainRenderTargetView = NULL; }
}

void CleanupDeviceD3D()
{
    CleanupRenderTarget();
    if (g_pSwapChain) { g_pSwapChain->Release(); g_pSwapChain = NULL; }
    if (g_pd3dDeviceContext) { g_pd3dDeviceContext->Release(); g_pd3dDeviceContext = NULL; }
    if (g_pd3dDevice) { g_pd3dDevice->Release(); g_pd3dDevice = NULL; }
}

/*
 * 防止鼠标穿透
 */
void ChangeClickability()
{
    long style = GetWindowLong(MyWnd, GWL_EXSTYLE);
    if (ShowMenu) {
        style &= ~WS_EX_LAYERED;
        SetWindowLong(MyWnd, GWL_EXSTYLE, style);
        SetForegroundWindow(MyWnd);
    }
    else {
        style |= WS_EX_LAYERED;
        SetWindowLong(MyWnd, GWL_EXSTYLE, style);
    }
}

LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
        return true;

    switch (msg)
    {
    case WM_SIZE:
        if (g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED)
        {
            ImGui_ImplDX11_InvalidateDeviceObjects();
            CleanupRenderTarget();
            g_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
            CreateRenderTarget();
            ImGui_ImplDX11_CreateDeviceObjects();
        }
        return 0;
    case WM_SYSCOMMAND:
        if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
            return 0;
        break;
    case WM_DESTROY:
        ::PostQuitMessage(0);
        exit(0);
        return 0;
    }
    return ::DefWindowProc(hWnd, msg, wParam, lParam);
}

HRESULT DirectXInit()
{
    DXGI_SWAP_CHAIN_DESC SwapChainDesc;
    ZeroMemory(&SwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
    SwapChainDesc.Windowed = TRUE;
    SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    SwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    SwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    SwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    SwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
    SwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    SwapChainDesc.BufferDesc.Height = 0;
    SwapChainDesc.BufferDesc.Width = 0;
    SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    SwapChainDesc.BufferCount = 1;
    SwapChainDesc.OutputWindow = MyWnd;
    SwapChainDesc.SampleDesc.Count = 8;
    SwapChainDesc.SampleDesc.Quality = 0;

    UINT createDeviceFlags = 0;
    D3D_FEATURE_LEVEL featureLevel;
    const D3D_FEATURE_LEVEL featureLevelArray[2] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0, };
    if (D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevelArray, 2, D3D11_SDK_VERSION, &SwapChainDesc, &g_pSwapChain, &g_pd3dDevice, &featureLevel, &g_pd3dDeviceContext) != S_OK)
        return false;

    CreateRenderTarget();
    SetWindowPos(MyWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
}

bool SetupWindow()
{
    LPCWSTR name = L"DirectX Screen Capture";

    WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_HREDRAW | CS_VREDRAW, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, LoadCursor(NULL, IDC_ARROW), NULL, NULL, name, NULL };

    RegisterClassEx(&wc);
    MyWnd = CreateWindow(wc.lpszClassName, name, WS_POPUP | WS_VISIBLE, 0, 0, ScreenWidth, ScreenHeight, NULL, NULL, wc.hInstance, NULL);

    MARGINS margin = { -1 };
    DwmExtendFrameIntoClientArea(MyWnd, &margin);
    SetMenu(MyWnd, NULL);
    SetWindowLongPtr(MyWnd, GWL_STYLE, WS_VISIBLE);
    SetWindowLongPtr(MyWnd, GWL_EXSTYLE, WS_EX_LAYERED | WS_EX_TRANSPARENT);

    ShowWindow(MyWnd, SW_SHOW);
    UpdateWindow(MyWnd);
    SetWindowLong(MyWnd, GWL_EXSTYLE, GetWindowLong(MyWnd, GWL_EXSTYLE) | WS_EX_LAYERED | WS_EX_TRANSPARENT);
    return true;
}

#pragma endregion


WPARAM Draw()
{
	ZeroMemory(&Message, sizeof(MSG));

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui_ImplWin32_Init(MyWnd);
    ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);
    ImGui::StyleColorsDark();
    UpdateWindow(MyWnd);
    SetForegroundWindow(MyWnd);

    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags = ImGuiConfigFlags_NoMouseCursorChange;

    io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\msyh.ttc", 17, NULL, io.Fonts->GetGlyphRangesChineseFull());
    io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\msyh.ttc", 17, NULL, io.Fonts->GetGlyphRangesChineseFull());
    io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\msyh.ttc", 17, NULL, io.Fonts->GetGlyphRangesChineseFull());


	while (Message.message != WM_QUIT)
	{
		if (PeekMessage(&Message, MyWnd, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&Message);
			DispatchMessage(&Message);
		}
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        POINT p;
        POINT xy;
        GetCursorPos(&p);
        io.MousePos.x = p.x;
        io.MousePos.y = p.y;

        if (GetAsyncKeyState(VK_LBUTTON)) {
            io.MouseDown[0] = true;
            io.MouseClicked[0] = true;
            io.MouseClickedPos[0].x = io.MousePos.x;
            io.MouseClickedPos[0].x = io.MousePos.y;
        }
        else
            io.MouseDown[0] = false;

		Render();
		Sleep(1);

        HWND DESKTOP = GetForegroundWindow();
        HWND MOVERDESK = GetWindow(DESKTOP, GW_HWNDPREV);
        SetWindowPos(MyWnd, MOVERDESK, NULL, NULL, NULL, NULL, SWP_NOMOVE | SWP_NOSIZE);
        UpdateWindow(MyWnd);
	}

	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	CleanupDeviceD3D();
	DestroyWindow(MyWnd);

	return Message.wParam;
}


void Render()
{
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	ImGui::SetNextWindowPos(ImVec2(20, 25));
	ImGui::SetNextWindowSize(ImVec2(750, 550));

    ImGuiIO& io = ImGui::GetIO();

    if (IsKeyDown(VK_INSERT) && !KeyIns)
    {
        ShowMenu = !ShowMenu;
        ChangeClickability();
        KeyIns = true;
    }
    else if (!IsKeyDown(VK_INSERT) && KeyIns)
    {
        KeyIns = false;
    }

    if (ShowMenu) {
        DrawMenu();
    }
    
	DrawEsp();

	ImGui::EndFrame();
	ImGui::Render();

	ImVec4 clear_color = ImVec4(0., 0., 0., 0.);
	g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
	g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, (float*)&clear_color);
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	g_pSwapChain->Present(1, 0);
}


void DrawMenu()
{
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, { 0, 0 });

    ImGui::Begin(skCrypt("menu"), 0, ImGuiWindowFlags_NoDecoration);
    {
        ImGui::PopStyleVar();

        ImGuiWindow* window = ImGui::GetCurrentWindow();

        ImDrawList* draw = ImGui::GetWindowDrawList();
        ImVec2 p = ImGui::GetWindowPos();

        static bool navbar_collapsed = true;
        static float navbar_width = 0.f; navbar_width = ImLerp(navbar_width, navbar_collapsed ? 0.f : 1.f, 0.04f);
        content_anim = ImLerp(content_anim, 1.f, 0.04f);

        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, !navbar_collapsed);
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, content_anim * (navbar_width > 0.005f ? 1 / navbar_width / 2 : 1.f));
        ImGui::SetCursorPos({ 81, 25 });
        ImGui::BeginGroup();
        ImGui::PushFont(ImGui::GetIO().Fonts->Fonts[2]);
        //ImGui::Text(skCrypt("Page 1"));
        ImGui::TextColored(PURPLE, skCrypt(u8"FPS:%.f"), ImGui::GetIO().Framerate);
        ImGui::SameLine();
        ImGui::TextColored(WHITE, skCrypt(u8"遗憾才是人间常态。"));
        ImGui::PopFont();

        ImGui::BeginChild(skCrypt("main"), { window->Size.x - 81, window->Size.y - window->DC.CursorPos.y + window->Pos.y }, 0, ImGuiWindowFlags_NoBackground);
        {
            switch (tab) {
            case 0: {
                ImGui::BeginChild(skCrypt("subtabs"), { ImGui::GetWindowWidth() - 30, 40 }, 1);
                {
                    ImGui::SetCursorPos({ 16, 0 });

                    if (custom::subtab(skCrypt("AimBot##subtab"), subtab == 0)) subtab = 0; ImGui::SameLine(0, 20);

                }
                ImGui::EndChild();

                ImGui::BeginChild(skCrypt("main_child"), { ImGui::GetWindowWidth(), ImGui::GetWindowHeight() - 55 }, 0, ImGuiWindowFlags_NoBackground);
                {
                    AimConfig config = GameConfig->trackConfig;

                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt(u8"自瞄配置"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];
                            
                            ImGui::Combo(skCrypt(u8"自瞄按键"), &track_key, skCrypt(u8"左键\0右键\0上侧键\0下侧键\0Ctrl\0Shift\0Alt\0"));
                            {
                                config.key_code = g_aim_key_mapping[track_key];
                            }

                            ImGui::SliderInt(skCrypt(u8"平滑"), &config.smooth, 1, 50);
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"越小越锁，1为拉满。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"自瞄距离"), &GameConfig->max_dist, 0, 300, u8"%.f米");

                            ImGui::SliderFloat(skCrypt(u8"后坐力系数"), &config.recoil, 0, 1, "%.2f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"0为全部后坐力，1为无后座，值越大后坐力越小"));
                                ImGui::EndTooltip();
                            }

                            ImGui::Combo(skCrypt(u8"自瞄部位"), &config.bone, skCrypt(u8"头\0脖\0胸\0腰\0G8\0"));
                        }
                        custom::end_child();
                    }
                    ImGui::EndGroup();
                    ImGui::SameLine(0, 15);
                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt(u8"自瞄动态FOV配置"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];

                            ImGui::SliderFloat(skCrypt(u8"最小Fov"), &config.min_fov, 1, 50, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个Fov的下限，算法会根据距离在最小Fov和最大Fov动态计算。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"最大Fov"), &config.max_fov, 1, 100, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个Fov的上限，算法会根据距离在最小Fov和最大Fov动态计算。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"激活最小Fov距离"), &config.active_min_distance, 1, 200, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个触发Fov的下限的距离，当目标和你的距离小于等于设置该距离时，使用最小Fov。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"激活最大Fov距离"), &config.active_max_distance, 1, 50, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个触发Fov的上限的距离，当目标和你的距离大于等于设置该距离时，使用最大Fov。"));
                                ImGui::EndTooltip();
                            }

                        }
                        custom::end_child();

                    }
                    ImGui::EndGroup();
                    ImGui::Spacing();
                }
                ImGui::EndChild();
            } break;
            }
        }
        ImGui::EndChild();

        ImGui::EndGroup();
        ImGui::PopStyleVar();
        ImGui::PopItemFlag();

        ImGui::SetCursorPos({ 0, 0 });


        ImGui::BeginChild(skCrypt("main"), { window->Size.x - 81, window->Size.y - window->DC.CursorPos.y + window->Pos.y }, 0, ImGuiWindowFlags_NoBackground);
        {
            switch (tab) {
            case 1: {
                ImGui::BeginChild(skCrypt("subtabs"), { ImGui::GetWindowWidth() - 30, 40 }, 1);
                {
                    ImGui::SetCursorPos({ 16, 0 });

                    if (custom::subtab(skCrypt("Trigger##subtab"), subtab == 1)) subtab = 1; ImGui::SameLine(0, 20);

                }
                ImGui::EndChild();

                ImGui::BeginChild(skCrypt("main_child"), { ImGui::GetWindowWidth(), ImGui::GetWindowHeight() - 55 }, 0, ImGuiWindowFlags_NoBackground);
                {
                    AimConfig config = GameConfig->flickConfig;
                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt(u8"扳机配置"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];

                            ImGui::Combo(skCrypt(u8"扳机按键"), &flick_key, skCrypt(u8"左键\0右键\0上侧键\0下侧键\0Ctrl\0Shift\0Alt\0"));
                            {
                                config.key_code = g_aim_key_mapping[flick_key];
                            }

                            ImGui::SliderInt(skCrypt(u8"平滑"), &config.smooth, 1, 50);
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"越小越锁，1为拉满。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"自瞄距离"), &GameConfig->max_dist, 0, 300, u8"%.f米");

                            ImGui::SliderFloat(skCrypt(u8"后坐力系数"), &config.recoil, 0, 1, "%.2f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"0为全部后坐力，1为无后座，值越大后坐力越小"));
                                ImGui::EndTooltip();
                            }

                            ImGui::Combo(skCrypt(u8"扳机部位"), &config.bone, skCrypt(u8"头\0脖\0胸\0腰\0G8\0"));
                            
                        }
                        custom::end_child();
                    }
                    ImGui::EndGroup();
                    ImGui::SameLine(0, 15);
                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt(u8"扳机动态FOV配置"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];

                            ImGui::SliderFloat(skCrypt(u8"最小Fov"), &config.min_fov, 1, 50, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个Fov的下限，算法会根据距离在最小Fov和最大Fov动态计算。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"最大Fov"), &config.max_fov, 1, 100, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个Fov的上限，算法会根据距离在最小Fov和最大Fov动态计算。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"激活最小Fov距离"), &config.active_min_distance, 1, 200, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个触发Fov的下限的距离，当目标和你的距离小于等于设置该距离时，使用最小Fov。"));
                                ImGui::EndTooltip();
                            }

                            ImGui::SliderFloat(skCrypt(u8"激活最大Fov距离"), &config.active_max_distance, 1, 50, "%.f");
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"动态Fov，设置一个触发Fov的上限的距离，当目标和你的距离大于等于设置该距离时，使用最大Fov。"));
                                ImGui::EndTooltip();
                            }
                        }
                        custom::end_child();

                    }
                    ImGui::EndGroup();
                    ImGui::Spacing();
                }
                ImGui::EndChild();
            } break;
            }
        }
        ImGui::EndChild();

        ImGui::BeginChild(skCrypt("main"), { window->Size.x - 81, window->Size.y - window->DC.CursorPos.y + window->Pos.y }, 0, ImGuiWindowFlags_NoBackground);
        {
            switch (tab) {
            case 2: {
                ImGui::BeginChild(skCrypt("subtabs"), { ImGui::GetWindowWidth() - 30, 40 }, 1);
                {
                    ImGui::SetCursorPos({ 16, 0 });

                    if (custom::subtab(skCrypt("Vision##subtab"), subtab == 2)) subtab = 2; ImGui::SameLine(0, 20);

                }
                ImGui::EndChild();

                ImGui::BeginChild(skCrypt("main_child"), { ImGui::GetWindowWidth(), ImGui::GetWindowHeight() - 55 }, 0, ImGuiWindowFlags_NoBackground);
                {
                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt("Misc"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];

                            ImGui::Checkbox(skCrypt(u8"发光"), &GameConfig->glowing);
                            ImGui::Checkbox(skCrypt(u8"绘制"), &GameConfig->esp);


                            ImGui::SliderFloat(skCrypt(u8"距离"), &GameConfig->max_dist, 10, 800, u8"%.f米");
                            ImGui::SliderFloat(skCrypt(u8"血条"), &GameConfig->healthbar_dist, 10, 300, u8"%.f米");


                            //ImGui::InputText("Text Field", text, 64);
                          //  ImGui::ColorEdit4("Color Edit", col, ImGuiColorEditFlags_NoBorder | ImGuiColorEditFlags_NoTooltip | ImGuiColorEditFlags_DisplayHex);
                        }
                        custom::end_child();
                    }
                    ImGui::EndGroup();
                    ImGui::SameLine(0, 15);
                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt("ESP"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];

                            ImGui::Checkbox(skCrypt(u8"方框"), &GameConfig->esp_box);
                            ImGui::Checkbox(skCrypt(u8"骨骼"), &GameConfig->esp_bone);
                            ImGui::Checkbox(skCrypt(u8"名字"), &GameConfig->esp_name);

                            ImGui::Checkbox(skCrypt(u8"距离"), &GameConfig->esp_distance);

                            ImGui::Checkbox(skCrypt(u8"护甲"), &GameConfig->esp_healthbar);
                            ImGui::Checkbox(skCrypt(u8"段位"), &GameConfig->esp_rank);
                            ImGui::Checkbox(skCrypt(u8"等级"), &GameConfig->esp_level);
                            ImGui::Checkbox(skCrypt(u8"击杀"), &GameConfig->esp_killcount);

                            // ImGui::InputText("Text Field##3", text, 64);
                            // ImGui::ColorEdit4("Color Edit##3", col, ImGuiColorEditFlags_NoBorder | ImGuiColorEditFlags_NoTooltip | ImGuiColorEditFlags_DisplayHex);
                            // ImGui::Button("Button##3");
                        }
                        custom::end_child();

                    }
                    ImGui::EndGroup();
                    ImGui::Spacing();
                }
                ImGui::EndChild();
            } break;
            }
        }
        ImGui::EndChild();

        ImGui::BeginChild(skCrypt("main"), { window->Size.x - 81, window->Size.y - window->DC.CursorPos.y + window->Pos.y }, 0, ImGuiWindowFlags_NoBackground);
        {
            switch (tab) {
            case 3: {
                ImGui::BeginChild(skCrypt("subtabs"), { ImGui::GetWindowWidth() - 30, 40 }, 1);
                {
                    ImGui::SetCursorPos({ 16, 0 });

                    if (custom::subtab(skCrypt("Misc##subtab"), subtab == 3)) subtab = 3; ImGui::SameLine(0, 20);

                }
                ImGui::EndChild();

                ImGui::BeginChild(skCrypt("main_child"), { ImGui::GetWindowWidth(), ImGui::GetWindowHeight() - 55 }, 0, ImGuiWindowFlags_NoBackground);
                {
                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt(u8"通用配置"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];

                            ImGui::Checkbox(skCrypt(u8"自瞄开关"), &GameConfig->aiming);
                            ImGui::Checkbox(skCrypt(u8"无后开关"), &GameConfig->aim_no_recoil);

                            ImGui::Checkbox(skCrypt(u8"靶场自动检测"), &GameConfig->auto_firing_range);
                            if (ImGui::IsItemHovered()) {
                                ImGui::BeginTooltip();
                                ImGui::Text(skCrypt(u8"开启后将自动检测是否在靶场内，也可以关闭自动检测手动勾选是否在靶场内。"));
                                ImGui::EndTooltip();
                            }
                            if (!GameConfig->auto_firing_range)
                            {
                                ImGui::Checkbox(skCrypt(u8"靶场"), &firing_range);
                            }
                            ImGui::Checkbox(skCrypt(u8"第三人称"), &GameConfig->thirdperson);
                            ImGui::Checkbox(skCrypt(u8"观战提醒"), &GameConfig->observer_reminder);
                            ImGui::Checkbox(skCrypt(u8"SuperGlide"), &GameConfig->super_glide);
                            ImGui::Checkbox(skCrypt(u8"战术换弹"), &GameConfig->auto_reload);

                        }
                        custom::end_child();
                    }
                    ImGui::EndGroup();
                    ImGui::Spacing();
                }
                ImGui::EndChild();
            } break;
            }
        }
        ImGui::EndChild();

        ImGui::BeginChild(skCrypt("main"), { window->Size.x - 81, window->Size.y - window->DC.CursorPos.y + window->Pos.y }, 0, ImGuiWindowFlags_NoBackground);
        {
            switch (tab) {
            case 4: {
                ImGui::BeginChild(skCrypt("subtabs"), { ImGui::GetWindowWidth() - 30, 40 }, 1);
                {
                    ImGui::SetCursorPos({ 16, 0 });

                    if (custom::subtab(skCrypt("Config##subtab"), subtab == 4)) subtab = 4; ImGui::SameLine(0, 20);
                    // if (custom::subtab("Trigger Bot##subtab", subtab == 1)) subtab = 1; ImGui::SameLine(0, 20);
                     // if (custom::subtab("Page 3##subtab", subtab == 2)) subtab = 2;
                }
                ImGui::EndChild();

                ImGui::BeginChild(skCrypt("main_child"), { ImGui::GetWindowWidth(), ImGui::GetWindowHeight() - 55 }, 0, ImGuiWindowFlags_NoBackground);
                {
                    ImGui::BeginGroup();
                    {
                        custom::begin_child(skCrypt(u8"保存参数"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });
                        {
                            static bool enabled = true;
                            static int slider = 0, combo = 0;
                            static char text[64];
                            static float col[4];

                            if (ImGui::Button(skCrypt(u8"保存")))
                            {
                                GameConfig->Save();
                            }
                        }
                        custom::end_child();
                        ImGui::EndGroup();
                        ImGui::SameLine(0, 15);
                        ImGui::BeginGroup();
                        custom::begin_child(skCrypt(u8"加载参数"), { (ImGui::GetWindowWidth() - 30) / 2 - ImGui::GetStyle().ItemSpacing.x / 2, 0 });

                        {
                            //Loading
                            if (ImGui::Button(skCrypt(u8"加载")))
                            {
                                GameConfig->Load();
                            }
                        }
                        custom::end_child();
                    }
                    ImGui::EndGroup();
                    ImGui::Spacing();
                }
                ImGui::EndChild();
            } break;
            }
        }
        ImGui::EndChild();


        ImGui::BeginChild(skCrypt("navbar"), { 50 + 100 * navbar_width, window->Size.y }, 0, ImGuiWindowFlags_NoBackground);
        {
            ImGui::GetWindowDrawList()->AddRectFilled(p, p + ImGui::GetWindowSize(), ImGui::GetColorU32(ImGuiCol_ChildBg), ImGui::GetStyle().WindowRounding, ImDrawFlags_RoundCornersLeft);
            ImGui::GetWindowDrawList()->AddRectFilled({ p.x + ImGui::GetWindowWidth() - 1, p.y }, p + ImGui::GetWindowSize(), ImGui::GetColorU32(ImGuiCol_Border));

            ImGui::SetCursorPosY(87);

            ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, { 0, 16 });
            if (custom::tab("A", skCrypt("Aimbot"), tab == 0)) tab = 0;
            if (custom::tab("T", skCrypt("Trigger"), tab == 1)) tab = 1;
            if (custom::tab("M", skCrypt("Misc"), tab == 3)) tab = 3;
            if (custom::tab("V", skCrypt("Vision"), tab == 2)) tab = 2;
            if (custom::tab("C", skCrypt("Config"), tab == 4)) tab = 4;
            ImGui::PopStyleVar();
        }
        ImGui::EndChild();

        ImGui::SetCursorPos({ 41.5f + 100 * navbar_width, 47 });
        if (custom::collapse_button(navbar_collapsed)) navbar_collapsed = !navbar_collapsed;
    }
    ImGui::End();
}

inline std::string GetRankName(int rankFlag)
{
    std::string rankName = "";
    switch (rankFlag) {
    case 1:
        rankName = skCrypt(u8"菜鸟").decrypt();
        break;
    case 2:
        rankName = skCrypt(u8"青铜").decrypt();
        break;
    case 3:
        rankName = skCrypt(u8"白银").decrypt();
        break;
    case 4:
        rankName = skCrypt(u8"黄金").decrypt();
        break;
    case 5:
        rankName = skCrypt(u8"铂金").decrypt();
        break;
    case 6:
        rankName = skCrypt(u8"钻石").decrypt();
        break;
    case 7:
        rankName = skCrypt(u8"大师").decrypt();
        break;
    case 8:
        rankName = skCrypt(u8"猎杀 #").decrypt();
        break;
    }
    return rankName;
}

/*
 * 绘制玩家方框
 */
inline void DrawPlayerBox(Player& player)
{
    if (GameConfig->esp_box)
    {
        if (player.visible)
        {
            if (player.knocked) {
                ImColor color = YELLOW;
                DrawBox(color, player.boxMiddle, player.h_y, player.width, player.height); //倒地颜色
            }
            else {
                ImColor color = RED;
                DrawBox(color, player.boxMiddle, player.h_y, player.width, player.height); //人物可见
            }
        }
        else
        {
            ImColor color = GREEN;
            DrawBox(color, player.boxMiddle, player.h_y, player.width, player.height);      //人物不可见
        }
    }
}

/*
 * 绘制玩家距离
 */
inline void DrawPlayerDistance(Player& player)
{
    if (GameConfig->esp_distance)
    {
        std::string distance = std::to_string(RealDist(player.dist));
        distance = distance.substr(0, distance.find('.')) + "m(" + std::to_string(player.entity_team) + ")";
        if (player.knocked) {
            ImColor color = YELLOW;
            String(ImVec2(player.boxMiddle - 8, (player.b_y + 1)), color, distance.c_str());  //倒地颜色
        }
        else {
            ImColor color = RED;
            String(ImVec2(player.boxMiddle - 8, (player.b_y + 1)), color, distance.c_str());  //距离颜色
        }
    }
}

/*
 * 绘制玩家血条
 */
inline void DrawPlayerHealthbar(Player& player)
{
    if (GameConfig->esp_healthbar)
    {
        if (RealDist(player.dist) < GameConfig->healthbar_dist)
        {
            DrawSeerLikeHealth((player.b_x - (player.width / 2.0f) + 5), (player.b_y - player.height - 10), player.shield, player.maxshield, player.armortype, player.health); //health bar
        }
    }
}

/*
 * 绘制玩家名称
 */
inline void DrawPlayerName(Player& player)
{
    if (GameConfig->esp_name)
    {
        String(ImVec2(player.boxMiddle - 0, (player.b_y - player.height - 16)), CYAN, player.name);
    }
}

/*
 * 绘制玩家段位
 */
inline void DrawPlayerRank(Player& player)
{
    if (GameConfig->esp_rank)
    {
        std::string rankName = GetRankName(player.rankFlag);
        if (rankName.size() > 0)
            String(ImVec2(player.boxMiddle - 9, (player.b_y + 14)), player.rankFlag == 8 ? RED : PURPLE, rankName.c_str());
    }
}

/*
 * 绘制玩家等级
 */
inline void DrawPlayerLevel(Player& player)
{
    if (GameConfig->esp_level)
    {
        std::string level = "[Lv" + std::to_string(player.level) + "]";
        String(ImVec2(player.boxMiddle - 44, (player.b_y - player.height - 16)), player.level < 100 ? PURPLE : CYAN, level.c_str());
    }
}

/*
 * 绘制玩家击杀数
 */
inline void DrawPlayerKillCount(Player& player)
{
    std::string k = "k." + std::to_string(player.killCount);
    String(ImVec2(player.boxMiddle + 20, (player.b_y + 15)), player.killCount >= 5 ? RED : PURPLE, k.c_str());	
}

/*
 * 绘制死亡观战玩家
 */
inline void DrawDeathPlayerName(Player& player, int& deathPlayerCount)
{
    if (player.entity_team == 0)
        return;

    String(ImVec2(400, 20 + (deathPlayerCount * 17)), YELLOW, player.name);
}


void DrawEsp()
{
    if (GameProcess.hBase != 0 && GameConfig->esp)
    {
        ImGui::SetNextWindowPos(ImVec2(0, 0));
        ImGui::SetNextWindowSize(ImVec2((float)ScreenWidth, (float)ScreenHeight));
        ImGui::Begin(skCrypt("##esp"), (bool*)true, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoBringToFrontOnFocus);

        int deathPlayerCount = 0;
        for (int i = 0; i < ReadEntitySize; i++)
        {
            Player player = g_players[i];
            if (player.health > 0)
            {
                if (RealDist(player.dist) > GameConfig->max_dist) continue;

                DrawPlayerBox(player);
                DrawPlayerDistance(player);
                DrawPlayerHealthbar(player);
                DrawPlayerName(player);
                DrawPlayerRank(player);
                DrawPlayerLevel(player);
                DrawPlayerKillCount(player);
            }
            else
            {
                DrawDeathPlayerName(player, deathPlayerCount);
            }
        }

        ImGui::End();
    }
}
