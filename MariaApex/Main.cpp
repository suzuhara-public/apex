#include "Main.h"
#include "API.h"

Process GameProcess;
Maria::Config* GameConfig;

// ##########全局临时变量###########
int track_key;
int flick_key;
bool firing_range = false;							// 是否开启靶场设置
uint32_t button_state[4];							// 游戏按键状态

static bool g_tmp_thirdperson = false;
static bool g_lock_enemy = false;					// 当前是否有锁定的目标
static float g_tmp_fov = 999.0f;					// 用于查找比该fov更小的目标
static uintptr_t aim_entity = 0;					// 最终要打击的目标
static uintptr_t tmp_aim_entity = 0;				// 每次循环所查找到的临时目标，根据fov比较取较小的一方
static uintptr_t last_aim_entity = 0;				// 之前锁定过的目标
float g_last_crosshair_target_time = 0.0f;			// 当前锁定目标最后在准心的时间

int g_current_fps = 0;								// 当前FPS

int enemy_spec = 0, team_spec = 0;					// 敌人观战数量，队友观战数量
int g_enemy_spec = 0, g_team_spec = 0;				// 临时敌人观战数量，临时队友观战数量

std::map<uintptr_t, float> g_last_visible_time_map;		// 可视
std::vector<std::string> g_weapon_names;				// 武器名称
uintptr_t g_player_entities[ReadEntitySize]{0};			// 玩家实体列表
Player g_players[ReadEntitySize];						// 玩家绘制数据
std::map<uintptr_t, PlayerDetail*> g_player_detail_map;	// 玩家段位数据

std::map<int, ButtonCode> g_aim_key_mapping;	// 玩家段位数据

bool g_in_lobby = true;									// 是否在大厅
bool g_should_load_player_detail = true;				// 是否应该加载玩家rank等数据

void LoadAimKeyMapping()
{
	g_aim_key_mapping[0] = ButtonCode::MOUSE_LEFT;
	g_aim_key_mapping[1] = ButtonCode::MOUSE_RIGHT;
	g_aim_key_mapping[2] = ButtonCode::MOUSE_5;
	g_aim_key_mapping[3] = ButtonCode::MOUSE_4;
	g_aim_key_mapping[4] = ButtonCode::KEY_LCONTROL;
	g_aim_key_mapping[5] = ButtonCode::KEY_LSHIFT;
	g_aim_key_mapping[6] = ButtonCode::KEY_LALT;
}


int GetKeyByValue(const ButtonCode& value) {
	for (const auto& pair : g_aim_key_mapping) {
		if (pair.second == value) {
			return pair.first;
		}
	}
	return 0;
}

// 动态FOV计算, 距离越近FOV越大越远FOV越小
inline float DynamicFOV(float distance, AimConfig& config) 
{
	if (distance <= config.active_max_distance) {  // 距离小于minDistance直接返回最大FOV
		return config.max_fov;
	}
	else if (distance >= config.active_min_distance) {  // 距离大于等于maxDistance直接返回最小FOV
		return config.min_fov;
	}
	else {
		// 距离在 (minDistance, maxDistance) 之间
		float avg = (config.max_fov - config.min_fov) / (config.active_min_distance - config.active_max_distance);
		return config.max_fov - (distance - config.active_max_distance) * avg;
	}
}

inline bool IsKeyDown(int vk)
{
	return (GetAsyncKeyState(vk) & 0x8000) != 0;
}


void SetPlayerGlow(Entity& localPlayer, Entity& target, int index) {
	if (GameConfig->glowing) {
		GColor color{};
		if (!firing_range && (target.isKnocked() || !target.isAlive())) {
			color = { 10.f, 10.f, 0.f };				//倒地， 黄色
		}
		else {
			float time = g_last_visible_time_map[target.ptr];
			float now = target.lastVisTime();
			if (now > time || (now < 0.f && time > 0.f))
			{
				if (target.getShield() == 0) {
					color = { 0.f, 15.f, 0.f };			// 打肉，绿色
				}
				else if (target.getShield() <= 50) {
					color = { 15.f, 15.f, 15.f };		// 白
				}
				else if (target.getShield() <= 75) {
					color = { 0.f, 15.f, 15.f };		// 蓝
				}
				else if (target.getShield() <= 100) {
					color = { 11.f, 2.f, 13.f };		// 紫
				}
				else if (target.getShield() <= 125) {
					color = { 15.f, 0.f, 0.f };			// 红
				}
			}
			else {
				color = { 13.f, 0.f, 7.f };				// 粉|紫色
			}
		}
		target.enableGlow(color);
	}
	else if (!GameConfig->glowing && target.isGlowing()) {
		target.disableGlow();
	}
}

inline void LoadStringTable()
{
	uint64_t table_ptr = Read<uint64_t>(GameProcess.hBase + OFFSET_WEAPON_NAMES, false);
	if (!table_ptr)
	{
		printf("table_ptr not found!\n");
		return;
	}
	CNetStringTable table = Read<CNetStringTable>(table_ptr, false);
	if (!table.items)
	{
		printf("table items not found!\n");
		return;
	}
	CNetStringDict dict = Read<CNetStringDict>(table.items, false);

	std::vector<std::string> temp_weapon_names;
	temp_weapon_names.resize(dict.used);
	char buffer[64];
	CNetStringTableItem item;
	for (size_t i = 0; i < dict.used; i += 1)
	{
		temp_weapon_names[i].clear();
		item = Read<CNetStringTableItem>(dict.elements + i * 72, false);
		if (!item.string)
			continue;
		ReadArray<char>(item.string, buffer, 64, false);
		temp_weapon_names[i].append(buffer);
		g_weapon_names = temp_weapon_names;
	}
}


/*
 * 处理玩家实体逻辑，查找自瞄要锁定的实体
 */
void ProcessEntity(Entity& localPlayer, Entity& target, uint64_t pEntityList, int index)
{
	g_player_entities[index] = target.ptr;
	if (!target.isAlive())
	{
		float localyaw = localPlayer.getYaw();
		float targetyaw = target.getYaw();

		if (localyaw == targetyaw)
		{
			if (localPlayer.isSameTeam(target))
				g_team_spec++;
			else
				g_enemy_spec++;
		}
		return;
	}

	Vector EntityPosition = target.getPosition();
	Vector LocalPlayerPosition = localPlayer.getPosition();
	float dist = LocalPlayerPosition.DistTo(EntityPosition);
	if (RealDist(dist) > GameConfig->max_dist) return;

	int target_team = target.getTeamId();

	if (!firing_range)
		if (target_team < 0 || target_team > 50 || localPlayer.isSameTeam(target)) return;

	if (GameConfig->aim_type == 2)
	{
		// 只考虑可视的敌人
		if (target.lastVisTime() > g_last_visible_time_map[target.ptr])
		{
			float fov = CalculateFov(localPlayer, target);
			if (fov < g_tmp_fov)
			{
				g_tmp_fov = fov;
				tmp_aim_entity = target.ptr;
			}
		}
		else
		{
			if (aim_entity == target.ptr)
			{
				aim_entity = tmp_aim_entity = last_aim_entity = 0;
			}
		}
	}
	else 
	{
		float fov = CalculateFov(localPlayer, target);
		if (fov < g_tmp_fov)
		{
			g_tmp_fov = fov;
			tmp_aim_entity = target.ptr;
		}
	}
	SetPlayerGlow(localPlayer, target, index);

	// 更新可视时间
	g_last_visible_time_map[target.ptr] = target.lastVisTime();
}

/*
 * 更新帧数
 */
inline void UpdateFrameCount()
{
	CGlobalVars vars = GetGlobalVars();
	static float framerate = 0;
	framerate = 0.9 * framerate + (1.0 - 0.9) * vars.absoluteframetime;
	g_current_fps = int(1.f / framerate);
	// printf("frametime = %f, fps = %d\n", vars.absoluteframetime, g_current_fps);
}

/*
 * 更新目标最后在十字准星的时间
 */
inline void UpdateLastCrosshairTargetTime(uintptr_t aimEntity)
{
	if (aimEntity == 0) {
		g_last_crosshair_target_time = 0;
		return;
	}
	Entity enemy = GetEntity(aimEntity);
	g_last_crosshair_target_time = enemy.getCrosshairTargetTime();
}

/*
 * 更新观战人数
 */
inline void UpdateObserverCount(std::chrono::time_point<std::chrono::system_clock>& spec_refresh_time)
{
	if (!team_spec && !enemy_spec)
	{
		team_spec = g_team_spec;
		enemy_spec = g_enemy_spec;
	}
	else
	{
		std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
		// 当前时间 - 上次刷新时间
		std::chrono::duration<double, std::milli> mills = now - spec_refresh_time;

		// 如果时间超过4秒，则刷新观战人数
		if (mills.count() > ObserverRefreshDurationTime)
		{
			team_spec = g_team_spec;
			enemy_spec = g_enemy_spec;
			spec_refresh_time = std::chrono::system_clock::now();
		}
	}
}

/*
 * 更新第三人称
 */
inline void UpdateThirdperson(uint64_t pLocalEntity)
{
	// 第三人称
	if (GameConfig->thirdperson && !g_tmp_thirdperson) {
		Write<int>(GameProcess.hBase + OFFSET_THIRDPERSON, 1);
		Write<int>(pLocalEntity + OFFSET_THIRDPERSON_SV, 1);
	}
	else if (!GameConfig->thirdperson && g_tmp_thirdperson)
	{
		Write<int>(GameProcess.hBase + OFFSET_THIRDPERSON, -1);
		Write<int>(pLocalEntity + OFFSET_THIRDPERSON_SV, 0);
		g_tmp_thirdperson = false;
	}
}

/*
 * 更新实体相关数据线程
 */
static void UpdateEntityThread()
{
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		// 观战刷新时间
		std::chrono::time_point<std::chrono::system_clock> spec_refresh_time = std::chrono::system_clock::now();

		while (GameProcess.hBase != 0) 
		{
			std::string levelName = GetLevelName();
			g_in_lobby = levelName == "mp_lobby";
			if (g_in_lobby)
			{
				g_in_lobby = true;
				g_last_visible_time_map.clear();
				memset(g_players, 0, sizeof(g_players));
				std::this_thread::sleep_for(std::chrono::milliseconds(10));
				continue;
			}
			// std::this_thread::sleep_for(std::chrono::milliseconds(5));
			uint64_t pLocalEntity = Read<uint64_t>(GameProcess.hBase + OFFSET_LOCAL_ENT, false);
			if (pLocalEntity == 0) continue;

			Entity LocalPlayer = GetEntity(pLocalEntity);
			int teamPlayer = LocalPlayer.getTeamId();
			if (teamPlayer < 0 || teamPlayer > 50) continue;

			uint64_t pEntityList = GameProcess.hBase + OFFSET_ENTITYLIST;

			// 尝试读取第一个实体，如果第一个都无法读取直接跳过本次循环
			uint64_t tmpBaseEntity = Read<uint64_t>(pEntityList);
			
			if (tmpBaseEntity == 0) continue;

			g_tmp_fov = 999.0f;
			tmp_aim_entity = 0;
			g_team_spec = 0;
			g_enemy_spec = 0;

			if (GameConfig->auto_firing_range)
			{
				firing_range = levelName == "mp_rr_canyonlands_staging_mu1";
			}
			
			int loopSize = firing_range ? FiringRangeReadEntitySize : ReadEntitySize;

			VMMDLL_SCATTER_HANDLE EL = Scatter_Initialize(VMMDLL_FLAG_NOCACHE | VMMDLL_FLAG_ZEROPAD_ON_FAIL);
			SPrepare(EL, GameProcess.hBase + OFFSET_ENTITYLIST, loopSize << 5);
			ExecuteRead(EL);

			//std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();

			if (firing_range) {
				int pindex = 0;
				for (int i = 0; i < FiringRangeReadEntitySize; i++)
				{
					uint64_t tmpEntity = SRead<uint64_t>(EL, pEntityList + ((uint64_t)i << 5));

					if (tmpEntity == 0 || pLocalEntity == tmpEntity) continue;

					if (!IsDummy(tmpEntity)) continue;

					Entity target = GetEntity(tmpEntity);

					ProcessEntity(LocalPlayer, target, pEntityList, pindex);
					pindex++;
				}
			}
			else 
			{
				for (int i = 0; i < ReadEntitySize; i++)
				{
					uint64_t tmpEntity = SRead<uint64_t>(EL, pEntityList + ((uint64_t)i << 5));

					if (tmpEntity == 0 || pLocalEntity == tmpEntity) continue;

					if (!IsPlayer(tmpEntity)) continue;

					Entity Target = GetEntity(tmpEntity);

					ProcessEntity(LocalPlayer, Target, pEntityList, i);
				}
			}

			SClear(EL, VMMDLL_FLAG_NOCACHE | VMMDLL_FLAG_ZEROPAD_ON_FAIL);

			 //std::chrono::time_point<std::chrono::system_clock> now2 = std::chrono::system_clock::now();
			 //std::chrono::duration<double, std::milli> mills = now2 - now;
			 //printf("%d entity mills = %ld (ms) \n", loopSize, (long)mills.count());

			if (g_lock_enemy)
				aim_entity = last_aim_entity;
			else
				aim_entity = tmp_aim_entity;

			UpdateLastCrosshairTargetTime(aim_entity);
			UpdateObserverCount(spec_refresh_time);
			UpdateThirdperson(pLocalEntity);
		}
	}
}


/*
 * 更新全局数据相关线程
 */
static void UpdateDataThread()
{
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		while (GameProcess.hBase != 0)
		{
			 std::this_thread::sleep_for(std::chrono::milliseconds(50));
			// 读取游戏按键状态
			ReadArray<uint32_t>(GameProcess.hBase + OFFSET_INPUT_SYSTEM + OFFSET_INPUT_BUTTON_STATE, button_state, sizeof(button_state));
			// std::cout << button_state[0] << "," << button_state[1] << "," << button_state[2] << "," << button_state[3] << std::endl;
			// 更新帧数
			UpdateFrameCount();
			LoadStringTable();
		}
	}
}

static void EspThread()
{
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		while (GameProcess.hBase != 0)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			if (GameConfig->esp)
			{
				uint64_t pLocalEntity = Read<uint64_t>(GameProcess.hBase + OFFSET_LOCAL_ENT, false);
				if (pLocalEntity == 0) continue;

				Entity localPlayer = GetEntity(pLocalEntity);
				Vector localPlayerPosition = localPlayer.getPosition();

				// 读矩阵
				uint64_t viewRenderer = Read<uint64_t>(GameProcess.hBase + OFFSET_RENDER, false);
				uint64_t viewMatrix = Read<uint64_t>(viewRenderer + OFFSET_MATRIX, false);
				Matrix m = Read<Matrix>(viewMatrix);

				for (int i = 0; i < ReadEntitySize; i++)
				{
					uintptr_t pTargetEntity = g_player_entities[i];
					// 无效玩家不绘制
					if (pTargetEntity == 0 || pTargetEntity == pLocalEntity) continue;
					Entity targetEntity = GetEntity(pTargetEntity);

					// 死亡玩家和队友不绘制
					if (!targetEntity.isAlive()) {
						g_players[i].health = 0;
						continue;
					}
					
					if (localPlayer.isSameTeam(targetEntity)) continue;

					Vector targetEntityPosition = targetEntity.getPosition();
					float dist = RealDist(localPlayerPosition.DistTo(targetEntityPosition));
					// 超出最大范围或者距离太近则不绘制
					if (dist > GameConfig->max_dist || dist < 50.0f) continue;

					Vector baseScreen = Vector();
					WorldToScreen(targetEntityPosition, m.matrix, ScreenWidth, ScreenHeight, baseScreen);

					if (baseScreen.x > 0 && baseScreen.y > 0)
					{
						Vector headScreen = Vector();
						Vector headPosition = targetEntity.getBonePositionByHitbox(0);
						WorldToScreen(headPosition, m.matrix, ScreenWidth, ScreenHeight, headScreen);
						float height = abs(abs(headScreen.y) - abs(baseScreen.y));
						float width = height / 2.0f;
						float boxMiddle = baseScreen.x - (width / 2.0f);
						int health = targetEntity.getHealth();
						int shield = targetEntity.getShield();
						int maxShield = targetEntity.getMaxShield();
						int armorType = targetEntity.getArmorType();
						uint64_t uid = targetEntity.getUid();

						int level = 0;
						int rankFlag = 0;
						int rankScore = 0;
						int killCount = 0;

						if (g_player_detail_map.count(uid) > 0)
						{
							PlayerDetail* detail = g_player_detail_map[uid];
							level = detail->level;
							rankFlag = detail->rankFlag;
							rankScore = detail->rankScore;
						}

						g_players[i] =
						{
							dist,
							targetEntity.getTeamId(),
							boxMiddle,
							headScreen.y,
							width,
							height,
							baseScreen.x,
							baseScreen.y,
							targetEntity.isKnocked(),
							(targetEntity.lastVisTime() > g_last_visible_time_map[pTargetEntity]),
							health,
							shield,
							maxShield,
							armorType,
							"未知",
							level,
							rankFlag,
							rankScore,
							killCount
						};
						targetEntity.getName(i - 1, &g_players[i].name[0]);
					}
				}
			}
		}
	}
}

static void LoadPlayerDetailThread()
{
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		while (GameProcess.hBase != 0)
		{
			uint64_t pLocalEntity = Read<uint64_t>(GameProcess.hBase + OFFSET_LOCAL_ENT, false);
			if (pLocalEntity == 0) continue;

			if (!g_in_lobby && g_should_load_player_detail)
			{
				Entity localPlayer = GetEntity(pLocalEntity);

				for (int i = 0; i < ReadEntitySize; i++)
				{
					uintptr_t pTargetEntity = g_player_entities[i];
					if (pTargetEntity == 0 || pTargetEntity == pLocalEntity) continue;

					Entity target = GetEntity(pTargetEntity);

					if (localPlayer.isSameTeam(target)) continue;

					uint64_t uid = Read<uint64_t>(pTargetEntity + OFFSET_USER_ID, false);
					if (g_player_detail_map.count(uid) != 0) continue;

					PlayerDetail* detail = GetPlayerDetail(uid);
					if (detail == nullptr)
						continue;		// 应该查询其他平台，而不仅仅是PC
					g_player_detail_map[uid] = detail;
				}
			}
			g_should_load_player_detail = false;
		}
	}
}

/*
 * 自瞄线程
 */
static void AimbotThread()
{
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		while (GameProcess.hBase != 0)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(1));

			if (!GameConfig->aiming) continue;
			
			if (aim_entity == 0)
			{
				g_lock_enemy = false;
				last_aim_entity = 0;
				continue;
			}
			uint64_t pLocalEntity = Read<uint64_t>(GameProcess.hBase + OFFSET_LOCAL_ENT, false);
			if (pLocalEntity == 0) continue;

			g_lock_enemy = true;
			last_aim_entity = aim_entity;

			Entity localPlayer = GetEntity(pLocalEntity);
			Entity target = GetEntity(aim_entity);

			// 自瞄类型
			AimType aimType = AimType::None;
			float fSmooth = 0;

			// 根据不同的自瞄按键选择不同的自瞄配置
			// using namespace Maria;
			AimConfig track = GameConfig->trackConfig;
			AimConfig flick = GameConfig->flickConfig;
			AimConfig* config = nullptr;

			if (IsButtonDown(flick.key_code)) {
				fSmooth = flick.smooth;
				aimType = AimType::Flick;
				config = &flick;
			} else if (IsButtonDown(track.key_code)) {
				fSmooth = track.smooth;
				aimType = AimType::Track;
				config = &track;
			}

			if (aimType == AimType::None) continue;

			float distance = localPlayer.getDistance(target);
			float fov = DynamicFOV(distance, *config);

			QAngle angles = CalculateBestBoneAim(localPlayer, aim_entity, config);
			if (angles.x == 0 && angles.y == 0)
			{
				g_lock_enemy = false;
				last_aim_entity = 0;
				continue;
			}

			localPlayer.setViewAngles(angles);

			if (aimType == AimType::Flick)
			{
				float crosshairTime = target.getCrosshairTargetTime();
				if (crosshairTime > g_last_crosshair_target_time) {
					Write<int>(GameProcess.hBase + OFFSET_IN_ATTACK + 0x8, 5);
					std::this_thread::sleep_for(std::chrono::milliseconds(8));
					Write<int>(GameProcess.hBase + OFFSET_IN_ATTACK + 0x8, 4);
				}
			}
		}
	}
}


/*
 * SuperGlide线程
 */
void ScriptThread()
{
	timeBeginPeriod(1);
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		while (GameProcess.hBase != 0) 
		{
			uint64_t pLocalEntity = Read<uint64_t>(GameProcess.hBase + OFFSET_LOCAL_ENT, false);
			if (pLocalEntity == 0) continue;

			if (GameConfig->auto_reload) AutoReload(pLocalEntity);
			if (GameConfig->super_glide) SuperGlide(pLocalEntity);

			Sleep(1);
		}
	}
	timeEndPeriod(1);
}

void DrawThread()
{
	SetupWindow();
	DirectXInit();
	Draw();
}

int main(int argc, char* argv[])
{
	// 加载按键映射
	LoadAimKeyMapping();

	// 加载配置文件
	{
		namespace fs = std::filesystem;
		fs::path dir_path = fs::current_path();
		fs::path config_path = dir_path.append("config.json");
		std::string path = config_path.string();
		GameConfig = new Maria::Config(config_path.string());
		if (!fs::exists(config_path))
		{
			GameConfig->Save();
		}
		GameConfig->Load();
		track_key = GetKeyByValue(GameConfig->trackConfig.key_code);
		flick_key = GetKeyByValue(GameConfig->flickConfig.key_code);
	}
	
	// 初始化DMA
	/*static const char* procName = skCrypt("r5apex.exe");
	static VMM_HANDLE hVmm = VMMDLL_Initialize();
	SetHvmm(hVmm);

	static DWORD pid = GetProcessPid(procName);

	if (pid == 0) {
		std::cout << skCrypt("Waiting To Open Game") << std::endl;
	}

	while (pid == 0) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1500));
		std::cout << ".";
		pid = GetProcessPid(procName);
	}

	SetProcessPid(pid);
	uint64_t hBase = GetModuleFromName(procName);*/

	// GameProcess = { hVmm, pid, 0 };
	GameProcess = { NULL, 0, 0 };

	std::thread draw_thr = std::thread(DrawThread);
	draw_thr.detach();

	std::thread actions_thr = std::thread(UpdateEntityThread);
	actions_thr.detach();

	std::thread data_thr = std::thread(UpdateDataThread);
	data_thr.detach();

	std::thread sg_thr = std::thread(ScriptThread);
	sg_thr.detach();

	std::thread aimbot_thr = std::thread(AimbotThread);
	aimbot_thr.detach();

	std::thread esp_thr = std::thread(EspThread);
	esp_thr.detach();

	std::thread detail_thr = std::thread(LoadPlayerDetailThread);
	detail_thr.detach();

	std::cout << skCrypt("Successfully Started !") << std::endl;

	while (true) {
		std::this_thread::sleep_for(std::chrono::milliseconds(5000));
		/*pid = GetProcessPid(procName);
		if (GameProcess.hProcess == pid)
			continue;

		std::cout << skCrypt("Waiting To Reopen Game") << std::endl;

		while (pid == 0) {
			g_should_load_player_detail = true;
			GameProcess.hBase = 0;
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			std::cout << ".";
			pid = GetProcessPid(procName);
		}

		std::cout << "\n" << skCrypt("Detected Apex Game") << std::endl;

		SetProcessPid(pid);
		GameProcess.hBase = GetModuleFromName(procName);
		GameProcess.hProcess = pid;

		std::cout << skCrypt("Successfully Started !") << std::endl;*/
	}
}