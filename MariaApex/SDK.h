#pragma once
#include <stdint.h>

typedef struct Matrix
{
    float matrix[16];
} Matrix;

typedef struct Bone
{
	uint8_t pad1[0xCC];
	float x;
	uint8_t pad2[0xC];
	float y;
	uint8_t pad3[0xC];
	float z;
} Bone;

struct GColor {
	float r, g, b;
};

struct GlowMode {
	int8_t GeneralGlowMode, BorderGlowMode, BorderSize, TransparentLevel;
};

struct Fade {
	int a, b;
	float c, d, e, f;
};

struct ClientClass {
	uint64_t pCreateFn;
	uint64_t pCreateEventFn;
	uint64_t pNetworkName;
	uint64_t pRecvTable;
	uint64_t pNext;
	uint32_t ClassID;
	uint32_t ClassSize;
};

struct NameEntry {
    uint64_t name1;
    uint64_t name2; // For whatever reason there's two name entries...
};

struct CNetStringTableItem {
    /*0x00*/ uint64_t unk00;
    /*0x08*/ uint64_t unk08;
    /*0x10*/ uint64_t string;
    /*0x18*/ uint64_t unk18;
    /*0x20*/ uint64_t unk20;
    /*0x28*/ uint64_t unk28;
    /*0x30*/ uint64_t unk30;
    /*0x38*/ uint64_t unk38;
    /*0x40*/ uint64_t unk40;
};
struct CNetStringDict {
    /*0x00*/ uint64_t vtable;
    /*0x08*/ uint64_t _unk08;
    /*0x10*/ uint64_t _unk10;
    /*0x18*/ uint64_t elements;
    /*0x20*/ uint16_t allocation_count;
    /*0x22*/ uint16_t grow_size;
    /*0x24*/ uint32_t _unk24;
    /*0x28*/ uint64_t _unk28;
    /*0x30*/ uint16_t _unk30;
    /*0x32*/ uint16_t used;
    /*0x34*/ uint16_t _unk34;
    /*0x36*/ uint16_t highest;
};
struct CNetStringTable {
    /*0x00*/ uint64_t vtable;
    /*0x08*/ int32_t table_id;
    /*0x0c*/ uint32_t table_id_pad;
    /*0x10*/ uint64_t table_name;
    /*0x18*/ int32_t max_entries;
    /*0x1c*/ int32_t entry_bits;
    /*0x20*/ int32_t tick_count;
    /*0x24*/ int32_t last_changed_tick;
    /*0x28*/ uint32_t flags;
    /*0x2c*/ int32_t user_data_size;
    /*0x30*/ int32_t user_data_size_bits;
    /*0x34*/ uint32_t pad;
    /*0x38*/ uint64_t change_func;
    /*0x40*/ uint64_t object;
    /*0x48*/ uint64_t items;
    /*0x50*/ uint64_t items_client_side;
};

// https://github.com/ValveSoftware/source-sdk-2013/blob/master/mp/src/public/globalvars_base.h
struct CGlobalVars {
	/*0x00*/double realtime;
	/*0x08*/int32_t framecount;
	/*0x0c*/float absoluteframetime;
	/*0x10*/float curtime;
	/*0x14*/float curtime2;
	/*0x18*/float curtime3;
	/*0x1c*/float curtime4;
	/*0x20*/float frametime;
	/*0x24*/float curtime5;
	/*0x28*/float curtime6;
	/*0x2c*/float zero;
	/*0x30*/float frametime2;
	/*0x34*/int32_t maxClients;
	/*0x38*/int32_t unk38;
	/*0x3c*/int32_t unk3C;
	/*0x40*/int32_t tickcount;
	/*0x44*/float interval_per_tick;
	/*0x48*/float interpolation_amount;
	// There's more stuff after this but I don't know and I don't care
};


enum class ButtonCode : uint32_t {
    KEY_FIRST = 0,
    KEY_NONE = KEY_FIRST,
    KEY_0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_A,
    KEY_B,
    KEY_C,
    KEY_D,
    KEY_E,
    KEY_F,
    KEY_G,
    KEY_H,
    KEY_I,
    KEY_J,
    KEY_K,
    KEY_L,
    KEY_M,
    KEY_N,
    KEY_O,
    KEY_P,
    KEY_Q,
    KEY_R,
    KEY_S,
    KEY_T,
    KEY_U,
    KEY_V,
    KEY_W,
    KEY_X,
    KEY_Y,
    KEY_Z,
    KEY_PAD_0,
    KEY_PAD_1,
    KEY_PAD_2,
    KEY_PAD_3,
    KEY_PAD_4,
    KEY_PAD_5,
    KEY_PAD_6,
    KEY_PAD_7,
    KEY_PAD_8,
    KEY_PAD_9,
    KEY_PAD_DIVIDE,
    KEY_PAD_MULTIPLY,
    KEY_PAD_MINUS,
    KEY_PAD_PLUS,
    KEY_PAD_ENTER,
    KEY_PAD_DECIMAL,
    KEY_LBRACKET,
    KEY_RBRACKET,
    KEY_SEMICOLON,
    KEY_APOSTROPHE,
    KEY_BACKQUOTE,
    KEY_COMMA,
    KEY_PERIOD,
    KEY_SLASH,
    KEY_BACKSLASH,
    KEY_MINUS,
    KEY_EQUAL,
    KEY_ENTER,
    KEY_SPACE,
    KEY_BACKSPACE,
    KEY_TAB,
    KEY_CAPSLOCK,
    KEY_NUMLOCK,
    KEY_ESCAPE = 70,
    KEY_SCROLLLOCK,
    KEY_INSERT,
    KEY_DELETE,
    KEY_HOME,
    KEY_END,
    KEY_PAGEUP,
    KEY_PAGEDOWN,
    KEY_BREAK,
    KEY_LSHIFT,
    KEY_RSHIFT = 80,
    KEY_LALT,
    KEY_RALT,
    KEY_LCONTROL,
    KEY_RCONTROL,
    KEY_LWIN,
    KEY_RWIN,
    KEY_APP,
    KEY_UP,
    KEY_LEFT,
    KEY_DOWN = 90,
    KEY_RIGHT,
    KEY_F1,
    KEY_F2,
    KEY_F3,
    KEY_F4,
    KEY_F5,
    KEY_F6,
    KEY_F7,
    KEY_F8,
    KEY_F9,
    KEY_F10,
    KEY_F11,
    KEY_F12,
    KEY_CAPSLOCKTOGGLE,
    KEY_NUMLOCKTOGGLE,
    KEY_SCROLLLOCKTOGGLE,

    KEY_LAST = KEY_SCROLLLOCKTOGGLE,
    // 107
    KEY_COUNT = KEY_LAST - KEY_FIRST + 1,

    // Mouse
    MOUSE_FIRST,

    MOUSE_LEFT = MOUSE_FIRST,
    MOUSE_RIGHT,
    MOUSE_MIDDLE,
    MOUSE_4,            // back
    MOUSE_5,            // forward
    MOUSE_WHEEL_UP,		// A fake button which is 'pressed' and 'released' when the wheel is moved up 
    MOUSE_WHEEL_DOWN,	// A fake button which is 'pressed' and 'released' when the wheel is moved down

    MOUSE_LAST = MOUSE_WHEEL_DOWN,
    MOUSE_COUNT = MOUSE_LAST - MOUSE_FIRST + 1,
};